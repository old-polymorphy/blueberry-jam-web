<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

// App
use App\Message;

// Framework
use Illuminate\Support\Facades\Broadcast;

Broadcast::channel('message-channel', static function (Message $message) {
    return $message->getAttribute('destination') == auth()->id();
});
