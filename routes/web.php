<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth routes
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::auth();

// Initial route
Route::get('/', static function () {
    return redirect('/adverts');
});

// Logout
Route::get('logout', static function () {
    Auth::logout();
    return redirect('/adverts');
});


// Displaying the user's profile
Route::get('profile/{user}', 'ProfileController@index')->name('profile');

// Displaying the community's profile
Route::get('profile/{user}/communities', 'ProfileController@communities')->name('community');

// Displaying the community's profile
Route::get('profile/{user}/bands', 'ProfileController@bands')->name('band');

// Route to the editing form
Route::get('profile/{user}/edit', 'ProfileController@edit')
    ->middleware('owner')
    ->name('profile');

// Update the user's profile
Route::put('profile/{user}', 'ProfileController@update');

// Displaying the user's profile
Route::get('profile/{user}/friends', 'ProfileController@friends')->name('friends');

// Displaying the user's profile
Route::post('profile/{user}/add', 'ProfileController@add');

//
Route::get('profile/search/{query}', 'ProfileController@search');

Route::get('notifications/{type}', 'ProfileController@notifications');


// Displaying the community's profile
Route::get('community/{community}', 'CommunityController@index')->name('community');

// Displaying the community's profile
Route::get('community/{community}/subscribers', 'CommunityController@subscribers')->name('community');

// Displaying the community's profile
Route::get('communities/create', 'CommunityController@create')->name('community');

// Displaying the community's profile
Route::get('community/{community}/subscribe', 'CommunityController@subscribe');

// Displaying the community's profile
Route::get('community/{community}/unsubscribe', 'CommunityController@unsubscribe');

// Add new band
Route::post('community', 'CommunityController@store');

// Delete the band
Route::delete('community/{community}/delete', 'CommunityController@destroy');

// Bands
Route::middleware('auth')
    ->get('communities/moderated', static function () {
        /** @var User $user */
        $user = auth()->user();

        return $user->moderatedCommunities();
    });


// Displaying the band's profile
Route::get('band/{band}', 'BandController@index')->name('band');

// Displaying the band's subscribers
Route::get('band/{band}/subscribers', 'BandController@subscribers')->name('band');

// The form for creating a band
Route::get('bands/create', 'BandController@create')->name('band');

// Subscribe to the band
Route::get('band/{band}/subscribe', 'BandController@subscribe');

// Unsubscribe to the band
Route::get('band/{band}/unsubscribe', 'BandController@unsubscribe');

// Add new band
Route::post('band', 'BandController@store');

// Update the band
Route::put('band/{band}', 'BandController@update');

// Delete the band
Route::delete('band/{band}/delete', 'BandController@destroy');

// Bands
Route::middleware('auth')
    ->get('bands/moderated', static function () {
        /** @var User $user */
        $user = auth()->user();

        return $user->moderatedBands();
    });


// Route to the editing form
Route::get('community/{community}/edit', 'CommunityController@edit')
    ->middleware('admin')
    ->name('community');

// Route to the editing form
Route::get('band/{band}/edit', 'BandController@edit')
    ->middleware('admin')
    ->name('band');


// Update the community's profile
Route::put('community/{community}', 'CommunityController@update');


// Update the community's profile
Route::put('band/{band}', 'BandController@update');


// Displaying all the adverts
Route::get('adverts', 'AdvertController@index')->name('adverts');

// Displaying the advert
Route::get('advert/{advert}', 'AdvertController@show')->name('adverts');

// Creating an advert page
Route::get('adverts/create', 'AdvertController@create')->name('adverts');

// Add the advert
Route::post('advert', 'AdvertController@store');

// Deleting the advert
Route::delete('advert/{advert}', 'AdvertController@delete');

// Get all adverts as JSON
Route::get('adverts/all', static function () {
    return response()->json(App\Advert::all()->sortByDesc('id')->values());
});


// Displaying all the posts
Route::get('posts', 'PostController@index')->name('posts');

// Displaying the post
Route::get('post/{post}', 'PostController@show')->name('posts');

// Creating a post page
Route::get('posts/create', 'PostController@create')->name('posts');

// Add the post
Route::post('post', 'PostController@store');

// Deleting the post
Route::delete('post/{post}', 'PostController@delete');

// Get all adverts as JSON
Route::get('posts/all', static function () {
    return response()->json(App\Post::all()->sortByDesc('id')->values());
});

// Messages
Route::get('dialogues', 'MessageController@dialogues');

Route::get('messages/{user}', 'MessageController@user');

Route::get('messages', 'MessageController@index')
    ->middleware('auth')->name('messages');

Route::get('dialogue/{destination}', 'MessageController@index')
    ->middleware('auth')->name('messages');

Route::post('read/{message}', 'MessageController@read');

Route::post('message', 'MessageController@store');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/me', 'ProfileController@id');

// Auth
Auth::routes();