<?php

namespace App;

use Exception;

// Framework
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Laravel\Passport\HasApiTokens;

/**
 * @property HasMany sent
 * @property HasMany received
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    /**
     * Appends attributes.
     *
     * @var array
     */
    protected $appends = [
        'last_message',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'first_name',
        'last_name',
        'city',
        'country',
        'picture',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the adverts published by the user given.
     *
     * @return HasMany
     */
    public function adverts(): HasMany
    {
        return $this->hasMany(Advert::class);
    }

    /**
     * Get the posts published by the user given.
     *
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Get the instruments played by the user given.
     *
     * @return BelongsToMany
     */
    public function instruments(): BelongsToMany
    {
        return $this->belongsToMany(Instrument::class);
    }

    /**
     * Get the genres preferred by the user given.
     *
     * @return BelongsToMany
     */
    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(Genre::class);
    }

    /**
     * Determine whether the user is online or not.
     *
     * @return boolean
     * @throws Exception
     */
    public function online(): bool
    {
        return cache()->has('online-' . $this->getAttribute('id'));
    }

    /**
     * Get bands the user listens to.
     *
     * @return BelongsToMany
     */
    public function bands(): BelongsToMany
    {
        return $this->belongsToMany(Band::class)->withPivot('role');
    }

    /**
     * Get communities the user subscribed to.
     *
     * @return BelongsToMany
     */
    public function communities(): BelongsToMany
    {
        return $this->belongsToMany(Community::class)->withPivot('role');
    }

    /**
     * Get users the user sent a request to.
     *
     * @return BelongsToMany
     */
    public function requests(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'friends', 'addressee', 'destination');
    }

    /**
     * Get subscribers of the user.
     *
     * @return BelongsToMany
     */
    public function subscribers(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'friends', 'destination', 'addressee');
    }

    /**
     * Get subscribers of the user.
     *
     * @return BelongsToMany
     */
    public function subscriptions(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'friends', 'addressee', 'destination');
    }

    /**
     * Get friends of the user.
     *
     * @return Collection
     */
    public function friends(): Collection
    {
        $requests = $this->requests()->get();
        $subscribers = $this->subscribers()->get();

        return $subscribers->intersect($requests);
    }

    /**
     * Returns all the bands created by the user.
     *
     * @return Collection
     */
    public function createdBands(): Collection
    {
        return $this->bands()->where('creator', $this->getAttribute('id'))->get();
    }

    /**
     * Displaying the bands administrated by the user.
     *
     * @return Collection
     */
    public function administratedBands(): Collection
    {
        $administrated = $this->belongsToMany(Band::class)->wherePivot('role', 2)->get();

        return $this->createdBands()->merge($administrated);
    }

    /**
     * Displaying the bands moderated by the user.
     *
     * @return Collection
     */
    public function moderatedBands(): Collection
    {
        $moderated = $this->belongsToMany(Band::class)->wherePivot('role', 1)->get();

        return $this->administratedBands()->merge($moderated);
    }

    /**
     * Displaying the communities created by the user.
     *
     * @return Collection
     */
    public function createdCommunities(): Collection
    {
        return $this->communities()->where('creator', $this->getAttribute('id'))->get();
    }

    /**
     * Displaying the communities administrated by the user.
     *
     * @return Collection
     */
    public function administratedCommunities(): Collection
    {
        $administrated = $this->belongsToMany(Community::class)->wherePivot('role', 2)->get();

        return $this->createdCommunities()->merge($administrated);
    }

    /**
     * Displaying the communities moderated by the user.
     *
     * @return Collection
     */
    public function moderatedCommunities(): Collection
    {
        $moderated = $this->belongsToMany(Community::class)->wherePivot('role', 1)->get();

        return $this->administratedCommunities()->merge($moderated);
    }

    /**
     * Returns all posts from the user's bands.
     *
     * @return array
     */
    public function bandPosts(): array
    {
        $bandIds = $this->bands()->pluck('id')->toArray();
        return Post::all()->whereIn('band_id', $bandIds)->all();
    }

    /**
     * Returns all posts from the user's communities.
     *
     * @return array
     */
    public function communityPosts(): array
    {
        $communityIds = $this->communities()->pluck('id')->toArray();
        return Post::all()->whereIn('community_id', $communityIds)->all();
    }

    /**
     * Returns all posts from the user's friends.
     *
     * @return array
     */
    public function friendPosts(): array
    {
        $friendsIds = array_merge([auth()->id()], $this->subscriptions()->pluck('id')->toArray());
        return Post::all()->whereIn('user_id', $friendsIds)->all();
    }

    /**
     * Returns all posts user subscribed to.
     *
     * @return object
     */
    public function feed(): object
    {
        $bandPosts = $this->bandPosts();
        $communityPosts = $this->communityPosts();
        $friendsPosts = $this->friendPosts();

        $posts = collect([$bandPosts, $communityPosts, $friendsPosts]);

        return $posts->collapse()->unique();
    }

    /**
     * All posts written by the user.
     *
     * @return Collection
     */
    public function myPosts(): Collection
    {
        return $this->posts()->orderByDesc('created_at')->get();
    }

    /**
     * All posts written by the user.
     *
     * @return Collection
     */
    public function myAdverts(): Collection
    {
        return $this->adverts()->orderByDesc('created_at')->get();
    }

    /**
     * Returns all sent messages.
     *
     * @return HasMany
     */
    public function sent(): HasMany
    {
        return $this->hasMany(Message::class, 'addressee');
    }

    /**
     * Returns all received messages.
     *
     * @return HasMany
     */
    public function received(): HasMany
    {
        return $this->hasMany(Message::class, 'destination');
    }

    /**
     * Returns all dialogue messages.
     *
     * @return Collection
     */
    public function messages(): Collection
    {
        $sent = $this->sent()->where('destination', auth()->id())->get();
        $received = $this->received()->where('addressee', auth()->id())->get();

        return collect([$sent, $received])->collapse()->unique()->sortBy('created_at');
    }

    /**
     * Returns all dialogues.
     *
     * @return mixed
     */
    public function dialogues()
    {
        $sent = $this->sent()->get()->pluck('destination');
        $received = $this->received()->get()->pluck('addressee');

        $ids = collect([$sent, $received])
            ->collapse()
            ->unique()
            ->values();

        return self::whereIn('id', $ids)
            ->select(['id', 'first_name', 'last_name', 'name', 'picture'])
            ->get();
    }

    /**
     * Returns all unread messages.
     *
     * @return Collection
     */
    public function unreadMessages(): Collection
    {
        return $this->received()->where('read', '=', false)->get();
    }

    /**
     * Appends the "last_message" attribute.
     *
     * @return mixed
     */
    public function getLastMessageAttribute()
    {
        return $this->messages()->last();
    }
}
