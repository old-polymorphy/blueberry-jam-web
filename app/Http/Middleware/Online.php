<?php

namespace App\Http\Middleware;

// Libs
use Carbon\Carbon;

// Framework
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// PHP
use Closure;
use Exception;

class Online
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request Request object.
     * @param Closure $next Closure object.
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $expiration = Carbon::now()->addMinutes(5);

            cache()->put('online-' . auth()->id(), true, $expiration);
        }

        return $next($request);
    }
}
