<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * TODO: REMOVE EXCEPTIONS
     *
     * @var array
     */
    protected $except = [
        '/message',
    ];
}
