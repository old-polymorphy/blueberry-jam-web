<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Owner
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request Request object.
     * @param Closure $next Closure object.
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user');

        if (!($user->id === auth()->id())) {
            return redirect('/');
        }

        return $next($request);
    }
}
