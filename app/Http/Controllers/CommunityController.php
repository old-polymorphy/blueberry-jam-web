<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCommunityForm;
use App\Community;
use App\Http\Requests\UpdateCommunityForm;

use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CommunityController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param Community $community
     * @return Factory|View
     */
    public function index(Community $community)
    {
        $users = $community->users()->inRandomOrder()->get();
        $editable = $community->editable();
        $posts = $community->posts()->get();

        return view('communities.index', compact(
            'community',
            'users',
            'editable',
            'posts'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('communities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddCommunityForm $form
     * @param Community $community
     * @return RedirectResponse
     */
    public function store(AddCommunityForm $form, Community $community): RedirectResponse
    {
        $form->persist($community);

        return redirect()
            ->action('CommunityController@index', ['id' => $community->getAttribute('id')])
            ->with('message', 'Band created! Add some more information');
    }

    /**
     * Display the specified resource.
     *
     * @param Community $community
     */
    public function show(Community $community)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Community $community
     * @return Factory|View
     */
    public function edit(Community $community)
    {
        return view('communities.edit', compact('community'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCommunityForm $form
     * @param Community $community
     * @return RedirectResponse
     */
    public function update(UpdateCommunityForm $form, Community $community): RedirectResponse
    {
        $form->persist($community);

        return redirect()
            ->action('CommunityController@index', $community->getAttribute('id'))
            ->with('message', 'Profile Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Community $community
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Community $community): RedirectResponse
    {
        // Delete the community's picture.
        if (
            $community->getAttribute('picture') &&
            file_exists('images/upload/' . $community->getAttribute('picture'))
        ) {
            unlink('images/upload/' . $community->getAttribute('picture'));
        }

        $community->users()->detach();
        $community->delete();

        return redirect()
            ->action('ProfileController@index', ['id' => auth()->id()])
            ->with('message', 'Community deleted');
    }

    /**
     * Show the community's subscribers.
     *
     * @param Community $community
     * @return Factory|View
     */
    public function subscribers(Community $community)
    {
        $users = $community->users()->get();
        $creator = $community->creator()->get();
        $admins = $community->admins()->get()->merge($creator);
        $moderators = $community->moderators()->get();

        return view('communities.subscribers', compact(
            'community',
            'users',
            'moderators',
            'admins'
        ));
    }

    /**
     * Subscribe to the community.
     *
     * @param Community $community
     * @return RedirectResponse
     */
    public function subscribe(Community $community): RedirectResponse
    {
        if (!$community->getAttribute('users')->contains(auth()->id())) {
            $community->users()->attach(auth()->id());
        }

        return redirect()->back();
    }

    /**
     * Unsubscribe to the community.
     *
     * @param Community $community
     * @return RedirectResponse
     */
    public function unsubscribe(Community $community): RedirectResponse
    {
        if ($community->getAttribute('users')->contains(auth()->id())) {
            $community->users()->detach(auth()->id());
        }

        return redirect()->back();
    }
}
