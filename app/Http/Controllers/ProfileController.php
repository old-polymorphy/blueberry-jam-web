<?php

namespace App\Http\Controllers;

// App
use App\Community;
use App\Band;
use App\User;
use App\Genre;
use App\Instrument;
use App\Http\Requests\UpdateProfileForm;

// Framework
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param User $user User object.
     * @return Factory|View
     */
    public function index(User $user): View
    {
        $adverts = $user->myAdverts()->values();
        $posts = $user->myPosts()->values();
        $friends = $user->friends();
        $bands = $user->bands()->inRandomOrder()->limit(4)->get();
        $communities = $user->communities()->inRandomOrder()->limit(4)->get();
        $editable = $user->getAttribute('id') === auth()->id();

        return view('profile.index', compact(
            'user',
            'adverts',
            'posts',
            'friends',
            'bands',
            'communities',
            'editable'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user User object.
     * @return Factory|View
     */
    public function edit(User $user): View
    {
        $instruments = Instrument::all();
        $genres = Genre::all();

        return view('profile.edit', compact('user', 'instruments', 'genres'));
    }

    /**
     * Update the profile.
     *
     * @param UpdateProfileForm $form Form object.
     * @param User $user User to update.
     * @return RedirectResponse
     */
    public function update(UpdateProfileForm $form, User $user): RedirectResponse
    {
        $form->persist($user);

        return redirect()
            ->action('ProfileController@index', auth()->id())
            ->with('message', 'Profile updated');
    }

    /**
     * Show friends, subscribers and requests of the user.
     *
     * @param User $user User to display friends.
     * @return Factory|View
     */
    public function friends(User $user): View
    {
        $friends = $user->friends();
        $subscribers = $user->subscribers()->get()->diff($friends);
        $requests = $user->requests()->get()->diff($friends);

        return view('profile.friends', compact('friends', 'subscribers', 'requests', 'user'));
    }

    /**
     * Display all the communities.
     *
     * @param User $user User to display communities.
     * @return Factory|View
     */
    public function communities(User $user): View
    {
        $communities = $user->communities()->orderBy('name')->get();
        $moderator = $user->moderatedCommunities()->sortBy('name');
        $administrator = $user->administratedCommunities()->sortBy('name');
        $creator = Community::all()->where('creator', auth()->id());

        return view('profile.communities', compact(
            'communities',
            'moderator',
            'administrator',
            'creator',
            'user'
        ));
    }

    /**
     * Display all the bands.
     *
     * @param User $user User to display bands.
     * @return Factory|View
     */
    public function bands(User $user): View
    {
        /**
         * Bar model (so the fucking hints work).
         *
         * @var Builder $bandModel
         */
        $bandModel = new Band();

        // Current user's bands
        $bands = $user->bands()->orderBy('name')->get();

        // Trending bands
        $trending = $bandModel->withCount('users')
            ->orderBy('users_count', 'desc')->get();

        // Bands moderated by the user
        $moderator = $user->moderatedBands()->sortBy('name');

        // Bands administrated by the user
        $administrator = $user->administratedBands()
            ->sortBy('name');

        // Bands created by the user
        $creator = $user->createdBands()->sortBy('name');

        return view('profile.bands', compact(
            'bands',
            'trending',
            'moderator',
            'administrator',
            'user',
            'creator'
        ));
    }


    /**
     * Add the user as a friend.
     *
     * @param User $user User to add/delete as a friend.
     * @return RedirectResponse
     */
    public function add(User $user): RedirectResponse
    {
        $authId = auth()->id();
        $subscribers = $user->subscribers();

        if ($user->getAttribute('subscribers')->contains($authId)) {
            $subscribers->detach($authId);
            $message = 'Request canceled!';
        } else {
            $subscribers->attach(auth()->id());
            $message = 'Request sent!';
        }

        return redirect()
            ->action('ProfileController@index', $user->getAttribute('id'))
            ->with('message', $message);
    }

    /**
     * Returns current user ID.
     *
     * @return int|null
     */
    public function id(): ?int
    {
        return auth()->id();
    }

    /**
     * Returns profiles by the search query.
     *
     * @param string $query — Search query.
     * @return mixed
     */
    public function search(string $query)
    {
        return DB::table('users')
            ->where('name', 'like', "%{$query}%")
            ->orWhere('first_name', 'like', "%{$query}%")
            ->orWhere('last_name', 'like', "%{$query}%")
            ->limit(4)
            ->get();
    }

    public function notifications(string $type)
    {
        function unread()
        {
            /** @var User $user */
            $user = auth()->user();

            if (!$user) {
                return '';
            }

            return $user->unreadMessages()->count();
        }

        switch ($type) {
            case 'messages':
                return response()->json(unread());
            default:
                return response()->json(null);
        }
    }
}
