<?php

namespace App\Http\Controllers;

// App
use App\Advert;
use App\Http\Requests\AddAdvertForm;

use Exception;

// Framework
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

class AdvertController extends Controller
{
    /**
     * Show all the adverts.
     *
     * @return Factory|View
     */
    public function index()
    {
        $adverts = Advert::all()->sortByDesc('created_at')->values();

        return view('adverts.index', compact('adverts'));
    }


    /**
     * Show the details of the given advert.
     *
     * @param Advert $advert Advert object.
     * @return Factory|View
     */
    public function show(Advert $advert)
    {
        return view('adverts.show', compact('advert'));
    }


    /**
     * Saving the advert to the database.
     *
     * @param AddAdvertForm $form Advert form.
     * @param Advert $advert Advert object.
     * @return JsonResponse
     */
    public function store(AddAdvertForm $form, Advert $advert): JsonResponse
    {
        return $form->persist($advert);
    }

    /**
     * Deletes the advert.
     *
     * @param Advert $advert
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function delete(Advert $advert): JsonResponse
    {
        $this->authorize('delete', $advert);

        try {
            $advert->delete();
            return response()->json($advert->getAttribute('id'));
        } catch (Exception $exception) {
            $exception->getMessage();
            return response()->json(false);
        }
    }


    /**
     * Showing the advert creating page.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('adverts.create');
    }
}
