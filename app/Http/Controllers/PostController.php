<?php

namespace App\Http\Controllers;

// App
use App\Post;
use App\User;
use App\Http\Requests\AddPostForm;

use Exception;

// Framework
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

class PostController extends Controller
{
    /**
     * Show all the posts.
     *
     * @return Factory|View
     */
    public function index(): View
    {
        /** @var User $user */
        $user = auth()->user();
        $posts = [];

        if ($user) {
            $posts = $user->feed()->sortByDesc('created_at')->values();
        }

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the details of the given post.
     *
     * @param Post $post
     * @return Factory|View
     */
    public function show(Post $post): View
    {
        return view('posts.show', compact('post'));
    }


    /**
     * Saving the post to the database.
     *
     * @param AddPostForm $form
     * @param Post $post
     * @return JsonResponse
     */
    public function store(AddPostForm $form, Post $post): JsonResponse
    {
        return $form->persist($post);
    }


    /**
     * Deletes the post.
     *
     * @param Post $post
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function delete(Post $post): JsonResponse
    {
        $this->authorize('delete', $post);

        try {
            $post->delete();
            return response()->json($post->getAttribute('id'));
        } catch (Exception $exception) {
            $exception->getMessage();
            return response()->json(false);
        }
    }


    /**
     * Showing the post creating page.
     *
     * @return View
     */
    public function create(): View
    {
        /** @var User $user Current user */
        $user = auth()->user();
        $bands = $user->administratedBands();

        return view('posts.create', compact('bands'));
    }
}
