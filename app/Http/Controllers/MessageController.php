<?php

namespace App\Http\Controllers;

use App\Events\MessageRead;
use App\Message;
use App\Http\Requests\AddMessageForm;
use App\User;
use Illuminate\Http\JsonResponse;

class MessageController extends Controller
{
    /**
     * Renders the messages page.
     *
     * @return mixed
     */
    public function index()
    {
        /** @var User $user */
        $user = auth()->user();

        if (!$user) {
            return redirect('/');
        }

        return view('profile.messages', compact('user'));
    }

    /**
     * Returns dialogues for the current user.
     *
     * @return JsonResponse
     */
    public function dialogues(): JsonResponse
    {
        /** @var User $user */
        $user = auth()->user();

        if ($user) {
            return response()->json($user->dialogues());
        }

        return response()->json(false);
    }

    // TODO: CHECK
    public function user(User $user): JsonResponse
    {
        return response()->json(
            $user->messages()->sortBy('created_at')->values()
        );
    }

    /**
     * Store a message.
     *
     * @param AddMessageForm $form — Form instance.
     * @param Message $message — Message object to store.
     * @return JsonResponse
     */
    public function store(AddMessageForm $form, Message $message): JsonResponse
    {
        return response()->json($form->persist($message));
    }

    /**
     * Marks the message as read and emits an event.
     *
     * @param Message $message — Message read.
     * @return JsonResponse
     */
    public function read(Message $message): JsonResponse
    {
        if ($message->getAttribute('destination') !== auth()->id()) {
            return response()->json(false);
        }

        $message->setAttribute('read', true);

        $result = $message->save();

        event(new MessageRead($message));

        return response()->json($result);
    }
}
