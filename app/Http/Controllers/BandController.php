<?php

namespace App\Http\Controllers;

// App
use App\Band;
use App\Http\Requests\AddBandForm;
use App\Http\Requests\UpdateBandForm;

// Framework
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

// PHP
use Exception;

class BandController extends Controller
{
    /**
     * Index method.
     *
     * @param Band $band
     * @return Factory|View
     */
    public function index(Band $band)
    {
        $users = $band->users()->inRandomOrder()->get();
        $editable = $band->editable();
        $adverts = $band->adverts()->get();
        $posts = $band->posts()->get();

        return view('bands.index', compact(
            'band',
            'editable',
            'adverts',
            'posts',
            'users'
        ));
    }

    /**
     * Show the form for creating a new band.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('bands.create');
    }

    /**
     * Store a newly created band in storage.
     *
     * @param AddBandForm $form
     * @param Band $band
     * @return RedirectResponse
     */
    public function store(AddBandForm $form, Band $band): RedirectResponse
    {
        $form->persist($band);

        return redirect()
            ->action('BandController@index', ['id' => $band->getAttribute('id')])
            ->with('message', 'Band created! Add some more information');
    }

    /**
     * Show the form for editing the band.
     *
     * @param Band $band
     * @return Factory|View
     */
    public function edit(Band $band)
    {
        return view('bands.edit', compact('band'));
    }

    /**
     * Update the specified band.
     *
     * @param UpdateBandForm $form
     * @param Band $band
     * @return RedirectResponse
     */
    public function update(UpdateBandForm $form, Band $band): RedirectResponse
    {
        $form->persist($band);

        return redirect()
            ->action('BandController@index', ['id' => $band->getAttribute('id')])
            ->with('message', 'Profile updated');
    }

    /**
     * Remove the specified band from storage.
     *
     * @param Band $band
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Band $band): RedirectResponse
    {
        // Delete the band's picture.
        if (
            $band->getAttribute('picture') &&
            file_exists('images/upload/' . $band->getAttribute('picture'))
        ) {
            unlink('images/upload/' . $band->getAttribute('picture'));
        }

        $band->users()->detach();
        $band->delete();

        return redirect()
            ->action('ProfileController@index', ['id' => auth()->id()])
            ->with('message', 'Band deleted');
    }

    /**
     * Show the band's subscribers.
     *
     * @param Band $band
     * @return Factory|View
     */
    public function subscribers(Band $band)
    {
        $users = $band->users()->get();
        $creator = $band->creator()->get();
        $admins = $band->admins()->get()->merge($creator);
        $moderators = $band->moderators()->get();

        return view('bands.subscribers', compact(
            'band',
            'users',
            'creator',
            'admins',
            'moderators'
        ));
    }

    /**
     * Subscribe to the band.
     *
     * @param Band $band
     * @return RedirectResponse
     */
    public function subscribe(Band $band): RedirectResponse
    {
        if (!$band->getAttribute('users')->contains(auth()->id())) {
            $band->users()->attach(auth()->id());
        }

        return redirect()->back();
    }

    /**
     * Unsubscribe to the band.
     *
     * @param Band $band
     * @return RedirectResponse
     */
    public function unsubscribe(Band $band): RedirectResponse
    {
        if ($band->getAttribute('users')->contains(auth()->id())) {
            $band->users()->detach(auth()->id());
        }

        return redirect()->back();
    }
}
