<?php

use App\User;
use Illuminate\Support\Facades\Route;

// Defining if the route is active
function isActive($page)
{
    if (Route::currentRouteName() === $page) {
        return ' active';
    }

    return '';
}

// Get name by id
function nameById($id)
{
    return User::findOrFail($id)->name;
}

/**
 * Returns the name to display.
 *
 * @param $firstName
 * @param $secondName
 * @return string
 */
function displayName($firstName, $secondName)
{
    if ($firstName) {
        if ($secondName) {
            $displayName = "{$firstName} {$secondName}";
        } else {
            $displayName = $firstName;
        }
    } elseif ($secondName) {
        $displayName = $secondName;
    } else {
        $displayName = null;
    }

    return $displayName;
}

/**
 * Displays the location.
 *
 * @param $city
 * @param $country
 * @return string
 */
function displayLocation($city, $country)
{
    if ($city) {
        if ($country) {
            $displayLocation = "{$city}, {$country}";
        } else {
            $displayLocation = $city;
        }
    } elseif ($country) {
        $displayLocation = $country;
    } else {
        $displayLocation = 'No location';
    }

    return $displayLocation;
}

/**
 * Is the user an administrator of the community.
 *
 * @param $entity
 * @return bool
 */
function isAdmin($entity)
{
    return $entity->admins->contains(auth()->id()) || ($entity->creator === auth()->id());
}

function unread()
{
    /** @var User $user */
    $user = auth()->user();

    if (!$user) {
        return '';
    }

    return $user->unreadMessages()->count();
}
