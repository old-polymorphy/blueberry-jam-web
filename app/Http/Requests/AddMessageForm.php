<?php

namespace App\Http\Requests;

// Libs
use App\Events\MessageSent;
use App\Message;

// Framework
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed destination
 * @property mixed body
 */
class AddMessageForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'destination' => 'required',
            'body' => 'required',
        ];
    }

    /**
     * Persists the form.
     *
     * @param Message $message — Message model instance.
     * @return mixed
     */
    public function persist(Message $message)
    {
        // Assign
        $message->setAttribute('addressee', auth()->id());
        $message->setAttribute('destination', $this->destination);
        $message->setAttribute('body', $this->body);

        // Save
        if ($message->save()) {
            event(new MessageSent($message));

            return $message;
        }

        return false;
    }
}
