<?php

namespace App\Http\Requests;

// App
use App\Post;

// Framework
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property bool as
 * @property string authorship
 * @property string title
 * @property string body
 */
class AddPostForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'title' => 'required|min:3|max:255',
            'body' => 'required',
        ];

        if ((bool)$this->as) {
            $rules['authorship'] = 'required';
        }

        return $rules;
    }

    /**
     * Add the post.
     *
     * @param Post $post Post object.
     * @return JsonResponse
     */
    public function persist(Post $post): JsonResponse
    {
        $post->setAttribute('title', $this->title);
        $post->setAttribute('body', $this->body);
        $post->setAttribute('user_id', auth()->id());

        if ($this->as === 'band') {
            $post->setAttribute('band_id', $this->authorship);
        }

        if ($this->as === 'community') {
            $post->setAttribute('community_id', $this->authorship);
        }

        if ($post->save()) {
            return response()->json($post);
        }

        return response()->json(false);
    }
}
