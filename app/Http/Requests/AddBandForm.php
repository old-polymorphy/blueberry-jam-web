<?php

namespace App\Http\Requests;

use App\Band;

// Libs
use Intervention\Image\ImageManager;

// Framework
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;

/**
 * @property mixed name
 * @property mixed bio
 * @property UploadedFile picture
 */
class AddBandForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:255|unique:bands',
            'bio' => 'required',
            'picture' => 'image',
        ];
    }

    /**
     * Persists the form.
     *
     * @param Band $band
     * @return void
     */
    public function persist(Band $band): void
    {
        // Assign
        $band->setAttribute('name', $this->name);
        $band->setAttribute('bio', $this->bio);
        $band->setAttribute('creator', auth()->id());

        // Setting picture
        if ($this->picture) {
            $extension = $this->picture->extension();
            $name = str_random(10) . '.' . $extension;
            $image = (new ImageManager)->make($this->picture);
            $image->save('images/upload/' . $name);
            $image->save('images/upload/thumbnails/' . $name, 20);
            $band->setAttribute('picture', $name);
        }

        // Save
        $band->save();

        // Set admin
        $band->admins()->attach(auth()->id(), ['role' => 2]);
    }
}
