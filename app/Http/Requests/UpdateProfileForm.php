<?php

namespace App\Http\Requests;

// App
use App\User;
use App\Genre;
use App\Instrument;

// Libs
use Intervention\Image\ImageManager;

// Framework
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

/**
 * @property string name
 * @property string first_name
 * @property string last_name
 * @property string city
 * @property string country
 * @property string bio
 * @property UploadedFile picture
 * @property bool delete_picture
 */
class UpdateProfileForm extends FormRequest
{
    /**
     * Determine if the user is authorized to update the profile.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        /** @var User $user */
        $user = $this->route('user');
        $authId = auth()->id();

        return $user->getAttribute('id') === $authId;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'min:3',
                'max:30',
                Rule::unique('users')->ignore(auth()->id()),
            ],
            'first_name' => 'nullable|regex:/^[\p{L}]*$/u',
            'last_name' => 'nullable|regex:/^[\p{L}]*$/u',
            'picture' => 'image',
        ];
    }

    /**
     * Updates the profile.
     *
     * @param User $user
     * @return void
     */
    public function persist(User $user): void
    {
        // Assign
        $this->assign($user);

        // Delete picture
        $this->deletePicture($user);

        // Setting picture
        if ($this->picture && !$this->delete_picture) {
            $this->addPicture($user);
        } elseif ($this->delete_picture) {
            $user->setAttribute('picture', null);
        }

        // Setting instruments
        $this->setInstruments($user);

        // Setting genres
        $this->setGenres($user);

        // Save changes
        $user->save();
    }

    /**
     * Assigns new values.
     *
     * @param User $user User to update.
     * @return void
     */
    private function assign(User $user): void
    {
        $user->setAttribute('name', $this->name);
        $user->setAttribute('first_name', $this->first_name);
        $user->setAttribute('last_name', $this->last_name);
        $user->setAttribute('city', $this->city);
        $user->setAttribute('country', $this->country);
        $user->setAttribute('bio', $this->bio);
    }

    /**
     * Adds a picture.
     *
     * @param User $user User to add a picture.
     * @return void
     */
    private function addPicture(User $user): void
    {
        $extension = '.' . $this->picture->extension();
        $name = str_random(10) . $extension;
        $image = (new ImageManager())->make($this->picture);
        $image->save('images/upload/' . $name);
        $image->save('images/upload/thumbnails/' . $name, 75);
        $user->setAttribute('picture', $name);
    }

    /**
     * Deletes the picture.
     *
     * @param User $user User to delete the picture.
     * @return void
     */
    private function deletePicture(User $user): void
    {
        $originalFilePath = 'images/upload/' . $user->getAttribute('picture');
        $thumbnailFilePath = 'images/upload/thumbnails/' . $user->getAttribute('picture');

        if (($this->picture || $this->delete_picture) && $user->getAttribute('picture')) {
            if (file_exists($originalFilePath)) {
                unlink($originalFilePath);
            }

            if (file_exists($thumbnailFilePath)) {
                unlink($thumbnailFilePath);
            }
        }
    }

    /**
     * Sets instruments.
     *
     * @param User $user User to add instruments.
     * @return void
     */
    private function setInstruments(User $user): void
    {
        $instruments = Instrument::all();
        foreach ($instruments as $instrument) {
            $name = $instrument->name;
            if ($this->$name) {
                if (!$user->getAttribute('instruments')->contains($instrument->id)) {
                    $user->instruments()->attach($instrument->id);
                }
            } else {
                $user->instruments()->detach($instrument->id);
            }
        }
    }

    /**
     * Sets genres.
     *
     * @param User $user User to add instruments.
     * @return void
     */
    private function setGenres(User $user): void
    {
        $genres = Genre::all();
        foreach ($genres as $genre) {
            $name = $genre->name;
            if ($this->$name) {
                if (!$user->getAttribute('genres')->contains($genre->id)) {
                    $user->genres()->attach($genre->id);
                }
            } else {
                $user->genres()->detach($genre->id);
            }
        }
    }
}
