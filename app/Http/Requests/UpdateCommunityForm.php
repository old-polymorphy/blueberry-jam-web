<?php

namespace App\Http\Requests;

use App\Community;

use Image;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCommunityForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $community = $this->route('community');
        return isAdmin($community);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $community = $this->route('community');
        return [
            'name' => [
                'min:3',
                'max:30',
                Rule::unique('communities')->ignore($community->id, 'id'),
            ],
            'picture' => 'image'
        ];
    }

    /**
     * Update the community.
     *
     * @param Community $community
     * @return void
     */
    public function persist(Community $community)
    {
        // Assign
        $community->name = $this->name;
        $community->city = $this->city;
        $community->country = $this->country;
        $community->bio = $this->bio;

        // Delete picture
        if ($community->picture && $this->delete_picture || $this->picture) {
            if (file_exists('images/upload/' . $this->picture)) {
                unlink('images/upload/' . $community->picture);
            }

            if (file_exists('images/upload/thumbnails/' . $this->picture)) {
                unlink('images/upload/thumbnails/' . $community->picture);
            }
        }

        // Setting picture
        if ($this->picture && !($this->delete_picture)) {
            $extension = $this->picture->extension();
            $name = str_random(10) . "." . $extension;
            $image = Image::make($this->picture);
            $image->save('images/upload/' . $name);
            $image->save('images/upload/thumbnails/' . $name, 10);
            $community->picture = $name;
        } elseif ($this->delete_picture) {
            $community->picture = null;
        }

        // Save changes
        $community->save();
    }
}
