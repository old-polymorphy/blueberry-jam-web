<?php

namespace App\Http\Requests;

// App
use App\Advert;

// Framework
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

/**
 * @property string type
 * @property string as
 * @property string title
 * @property string body
 * @property string band
 */
class AddAdvertForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'title' => 'required|min:3|max:255',
            'body' => 'required',
            'type' => 'required',
        ];

        if (!(int)$this->type && $this->as === 'true') {
            $rules['band'] = 'required';
        }

        return $rules;
    }

    /**
     * Add the post.
     *
     * @param Advert $advert Advert to save.
     * @return JsonResponse
     */
    public function persist(Advert $advert): JsonResponse
    {
        $advert->setAttribute('title', $this->title);
        $advert->setAttribute('body', $this->body);
        $advert->setAttribute('type', $this->type);
        $advert->setAttribute('user_id', auth()->id());

        if (!$this->type && $this->as) {
            $advert->setAttribute('band_id', $this->band);
        }

        if ($advert->save()) {
            return response()->json($advert);
        }

        return response()->json(false);
    }
}
