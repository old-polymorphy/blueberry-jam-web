<?php

namespace App\Http\Requests;

use App\Band;

use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\ImageManager;

/**
 * @property mixed name
 * @property mixed city
 * @property mixed country
 * @property mixed bio
 * @property UploadedFile picture
 * @property mixed delete_picture
 */
class UpdateBandForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $band = $this->route('band');

        return isAdmin($band);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $band = $this->route('band');
        return [
            'name' => [
                'min:3',
                'max:30',
                Rule::unique('communities')->ignore($band->id),
            ],
            'picture' => 'image',
        ];
    }

    /**
     * Update the community.
     *
     * @param Band $band Band to update.
     * @return void
     */
    public function persist(Band $band): void
    {
        // Assign
        $band->setAttribute('name', $this->name);
        $band->setAttribute('city', $this->city);
        $band->setAttribute('country', $this->country);
        $band->setAttribute('bio', $this->bio);

        // Delete picture
        if ($this->picture || ($band->getAttribute('picture') && $this->delete_picture)) {
            if (file_exists('images/upload/' . $this->picture)) {
                unlink('images/upload/' . $band->getAttribute('picture'));
            }

            if (file_exists('images/upload/thumbnails/' . $this->picture)) {
                unlink('images/upload/thumbnails/' . $band->getAttribute('picture'));
            }
        }

        // Setting picture
        if ($this->picture && !$this->delete_picture) {
            $extension = $this->picture->extension();
            $name = str_random(10) . '.' . $extension;
            $image = (new ImageManager())->make($this->picture);
            $image->save('images/upload/' . $name);
            $image->save('images/upload/thumbnails/' . $name, 20);
            $band->setAttribute('picture', $name);
        } elseif ($this->delete_picture) {
            $band->setAttribute('picture', null);
        }

        // Save changes
        $band->save();
    }
}
