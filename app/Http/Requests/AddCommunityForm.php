<?php

namespace App\Http\Requests;

// App
use App\Community;

// Libs
use Intervention\Image\ImageManager;

// Framework
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;

/**
 * @property mixed name
 * @property mixed bio
 * @property UploadedFile picture
 */
class AddCommunityForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|max:255|unique:bands',
            'bio' => 'required',
            'picture' => 'image',
        ];
    }

    /**
     * Persists the form.
     *
     * @param Community $community
     * @return void
     */
    public function persist(Community $community): void
    {
        // Assign
        $community->setAttribute('name', $this->name);
        $community->setAttribute('bio', $this->bio);
        $community->setAttribute('creator', auth()->id());

        // Setting picture
        if ($this->picture) {
            $extension = $this->picture->extension();
            $name = str_random(10) . '.' . $extension;
            $image = (new ImageManager)->make($this->picture);
            $image->save('images/upload/' . $name);
            $image->save('images/upload/thumbnails/' . $name, 20);
            $community->setAttribute('picture', $name);
        }

        // Save
        $community->save();

        // Set admin
        $community->admins()->attach(auth()->id(), ['role' => 2]);
    }
}
