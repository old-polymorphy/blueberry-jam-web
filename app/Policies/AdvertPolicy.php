<?php

namespace App\Policies;

use App\User;
use App\Advert;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdvertPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the advert.
     *
     * @param User $user
     * @param Advert $advert
     * @return mixed
     */
    public function view(User $user, Advert $advert)
    {
        //
    }

    /**
     * Determine whether the user can create adverts.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the advert.
     *
     * @param User $user
     * @param Advert $advert
     * @return mixed
     */
    public function update(User $user, Advert $advert)
    {
        //
    }

    /**
     * Determine whether the user can delete the advert.
     *
     * @param User $user
     * @param Advert $advert
     * @return bool
     */
    public function delete(User $user, Advert $advert): bool
    {
        return $user->getAttribute('id') === $advert->getAttribute('user_id');
    }
}
