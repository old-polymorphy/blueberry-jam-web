<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Advert extends Model
{
    /**
     * Appends attributes.
     */
    protected $appends = [
        'band',
        'user',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'type',
        'body',
        'user_id',
        'band_id',
    ];

    /**
     * Select the users owning the advert.
     *
     * @return Model|BelongsTo|object|null
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->first();
    }

    /**
     * Select the users owning the advert.
     *
     * @return Model|BelongsTo|object|null
     */
    public function band()
    {
        return $this->belongsTo(Band::class, 'band_id')->first();
    }

    /**
     * Returns the band.
     *
     * @return Model|BelongsTo|object|null
     */
    public function getBandAttribute()
    {
        return $this->band();
    }

    /**
     * Returns the user.
     *
     * @return Model|BelongsTo|object|null
     */
    public function getUserAttribute()
    {
        return $this->user();
    }
}
