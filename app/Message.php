<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Message extends Model
{
    public function addressee(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'addressee');
    }
}
