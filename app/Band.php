<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Band extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'password',
        'city',
        'country',
        'bio',
        'picture',
    ];

    /**
     * Get the users listening to the band.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get the users listening to the band.
     *
     * @return HasMany
     */
    public function adverts(): HasMany
    {
        return $this->hasMany(Advert::class)->orderByDesc('created_at');
    }

    /**
     * Gets the posts published by the band.
     *
     * @return HasMany
     */
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class)->orderByDesc('created_at');
    }

    /**
     * Get the moderators of the band.
     *
     * @return BelongsToMany
     */
    public function moderators(): BelongsToMany
    {
        return $this->belongsToMany(User::class)->wherePivot('role', 1);
    }

    /**
     * Get the admins of the band.
     *
     * @return BelongsToMany
     */
    public function admins(): BelongsToMany
    {
        return $this->belongsToMany(User::class)->wherePivot('role', 2);
    }

    /**
     * Returns the user who created the band.
     *
     * @return HasOne
     */
    public function creator(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'creator');
    }

    /**
     * Check whether the current user is allowed to edit the band.
     *
     * @return bool
     */
    public function editable(): bool
    {
        $creator = $this->creator()->get();
        $admins = $this->admins()->get()->merge($creator);
        $moderators = $this->moderators()->get()->merge($admins);

        return $moderators->contains(auth()->id());
    }
}
