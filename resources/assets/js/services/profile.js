// Libs
import axios from 'axios';

export default {
  getUserMessages(userId) {
    return axios.get(`/messages/${userId}`);
  },

  sendMessage(formData) {
    return axios.post('/message', formData);
  },

  myMessages() {
    return axios.get('/dialogues');
  },

  readMessage(messageId) {
    return axios.post(`/read/${messageId}`);
  },

  id() {
    return axios.get('/me');
  },

  search(query) {
    return axios.get(`/profile/search/${query}`);
  },

  notifications(type) {
    return axios.get(`/notifications/${type}`);
  }
};