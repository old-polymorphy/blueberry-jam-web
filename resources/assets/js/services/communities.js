// Libs
import axios from 'axios';

export default {
  /**
   * Get all bands moderated by the user.
   *
   * @return {AxiosPromise}
   */
  getModeratedCommunities() {
    return axios.get('/communities/moderated/');
  }
};