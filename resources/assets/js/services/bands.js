// Libs
import axios from 'axios';

export default {
  /**
   * Get all bands administrated by the user.
   *
   * @return {AxiosPromise}
   */
  getModeratedBands() {
    return axios.get('/bands/moderated/');
  }
};