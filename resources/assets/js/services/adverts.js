// Libs
import axios from 'axios';

export default {
  /**
   * Fetches all the adverts.
   *
   * @return {AxiosPromise}
   */
  getAdverts() {
    return axios.get('/adverts/all/');
  },

  /**
   * Creates an advert.
   *
   * @param {FormData} formData — FormData object.
   * @return {AxiosPromise}
   */
  createAdvert(formData) {
    return axios.post('/advert', formData);
  },

  /**
   * Deletes the advert.
   *
   * @param {number} advertId
   * @return {AxiosPromise}
   */
  deleteAdvert(advertId) {
    return axios.delete(`/advert/${advertId}/`);
  }
};