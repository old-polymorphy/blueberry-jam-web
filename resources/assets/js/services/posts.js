// Libs
import axios from 'axios';

export default {
  /**
   * Fetches all the posts.
   *
   * @return {AxiosPromise}
   */
  getPosts() {
    return axios.get('/posts/all/');
  },

  /**
   * Creates a post.
   *
   * @param {FormData} formData — FormData object.
   * @return {AxiosPromise}
   */
  createPost(formData) {
    return axios.post('/post/', formData);
  },

  /**
   * Deletes the post.
   *
   * @param {number} postId — Post numeric ID.
   * @return {AxiosPromise}
   */
  deletePost(postId) {
    return axios.delete(`/post/${postId}/`);
  },
};
