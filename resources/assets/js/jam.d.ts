declare interface Publication {
  band?: any;
  band_id?: number;
  body: string;
  created_at: string;
  community?: any;
  community_id?: number;
  id: number;
  title: string;
  type: number;
  updated_at: string;
  user?: User;
  user_id?: number;
}

declare interface Post extends Publication {}

declare interface Advert extends Publication {
  type: number;
}

declare interface User {
  ban?: string;
  bio?: string;
  city?: string;
  country?: string;
  created_at: string;
  email: string;
  first_name?: string;
  id: number;
  last_name?: string;
  last_message: Message;
  name: string;
  picture?: string;
  type: number;
  updated_at: string;
}

declare interface Dialogue {
  first_name?: string;
  id: number;
  last_name?: string;
  last_message: Message;
  name: string;
  picture?: string;
}

declare interface Message {
  id: number;
  addressee: number;
  destination: number;
  created_at: string;
  updated_at: string;
  body: string;
  read: boolean;
}