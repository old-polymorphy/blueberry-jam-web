// Bootstrap files
import './bootstrap';

// Vue
import Vue from 'vue';

// Plugins
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';

Vue.use(VueRouter, VueResource);

// Components
import ProfilePicture from './components/profile-picture/profile-picture.vue';
import AdvertsPage from './components/adverts-page/adverts-page.vue';
import FriendSearcher from './components/friend-searcher/friend-searcher.vue';
import PostsPage from './components/posts-page/posts-page.vue';
import AddAdvert from './components/add-advert/add-advert.vue';
import AddPost from './components/add-post/add-post.vue';
import MessageElement from './components/message-element/message-element';
import MessageForm from './components/message-form/message-form.vue';
import MessagePage from './components/message-page/message-page.vue';
import NewsCounter from './components/news-counter/news-counter.vue';

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      component: MessageForm,
      name: 'dialogue',
      path: '/dialogue/:destination',
    },
    {
      component: MessageForm,
      name: 'profile',
      path: '/profile/:destination',
    },
    {
      path: '/messages',
      component: MessagePage,
    },
  ]
});

new Vue({
  router,
  el: '#app',

  components: {
    AddAdvert,
    AddPost,
    AdvertsPage,
    FriendSearcher,
    PostsPage,
    ProfilePicture,
    MessageElement,
    MessageForm,
    MessagePage,
    NewsCounter,
  }
});

document.addEventListener('DOMContentLoaded', () => {

  // Profile tabs
  document.querySelectorAll('.profile-navigation button')
    .forEach(button => {
      button.addEventListener('click', function () {
        if (!this.classList.contains('active')) {
          document.querySelector('button.active')
            .classList.remove('active');

          this.classList.add('active');
        }

        const name = this.innerText.toLowerCase();

        document.querySelectorAll('.profile-tab')
          .forEach(tab => tab.classList.add('hidden'));

        document.querySelector('.profile-' + name + '-tab')
          .classList.remove('hidden');
      });
    });

  // File on change
  const customFileButton = document
    .querySelectorAll('.customFile, .customFile-text');

  if (customFileButton) {
    customFileButton.forEach((el) => {
      el.addEventListener('click', handleCustomFileClick);
    });
  }

  // Custom file button
  const customFileInput = document.querySelector('.customFile-input');

  if (customFileInput) {
    customFileInput.addEventListener('change', handleCustomFileChange);
  }

  // Delete picture button
  const deletePictureButton = document
    .querySelector('.editForm-deletePictureButton');

  if (deletePictureButton) {
    deletePictureButton.addEventListener('click', () => {
      const form = document.querySelector('.editForm');
      const input = document.createElement('input');

      input.setAttribute('type', 'checkbox');
      input.setAttribute('name', 'delete_picture');
      input.setAttribute('checked', 'true');

      form.append(input);
      form.submit();
    });
  }

  // Add friend
  document
    .querySelectorAll('.addFriend, .cancelFriend, .deleteFriend')
    .forEach((el) => el.addEventListener('click', () => {
      document.querySelector('.profile-friendForm').submit();
    }));

  // Flash message
  const flashMessage = document.querySelector('.flash');

  if (flashMessage) {
    setInterval(() => {
      flashMessage.classList.add('flash-hidden');
    }, 2000);

    flashMessage
      .addEventListener('transitionend', flashMessage.remove);
  }

  // Hamburger
  const hamburger = document.querySelector('.navHamburger');
  const leftNavigation = document.querySelector('.leftNavigation');
  const appContent = document.getElementById('app');

  if (hamburger && leftNavigation && appContent) {
    hamburger.addEventListener('click', () => {
      hamburger.classList.toggle('is-active');
      leftNavigation.classList.toggle('leftNavigation-active');
      appContent.classList.toggle('app-active');
    });
  }

});

/**
 * Handles custom file button click.
 *
 * @param {Event} e — Event object.
 * @returns {void}
 */
function handleCustomFileClick(e) {
  if (e.target === e.currentTarget) {
    const input = document.querySelector('.customFile-input');

    if (input) {
      input.click();
    }
  }
}

/**
 * Handles custom file change.
 * Sets innerText of the input element.
 *
 * @returns {void}
 */
function handleCustomFileChange() {
  const customFile = this.files[0];
  const customFileButtonText = document.querySelector('.customFile-text');

  if (customFile && customFileButtonText) {
    customFileButtonText.innerText = customFile.name;
  } else {
    customFileButtonText.innerText = 'Choose File';
  }
}