// Libs
import moment from 'moment';

// Services
import profileService from '../../services/profile';

// Components
import MessageForm from '../../components/message-form/message-form.vue';

export default {
  name: 'message-page',

  components: {MessageForm},

  data() {
    return {
      authId: null,
      dialogues: [],
      pending: false,
    };
  },

  filters: {
    /**
     * Formats the date.
     *
     * @param {string} dateString — Raw date string.
     * @returns {string}
     */
    creationDate(dateString) {
      return moment(dateString).calendar().toLowerCase();
    }
  },

  methods: {
    /**
     * Subscribes to a channel.
     *
     * @return {void}
     */
    subscribe() {
      // Echo channel
      const channel = window.Echo.channel(`message-channel-${this.authId}`);

      channel.listen('MessageSent', (data) => {
        const dialogue = this.getDialogueByAddressee(data.message.addressee);
        dialogue.last_message = data.message;
        this.dialogues = this.dialogues.sort(this.sortDialogues);
      });
    },

    /**
     * Finds dialogue by addressee numeric ID.
     *
     * @param {number} addressee — Addressee numeric ID.
     * @return {Dialogue}
     */
    getDialogueByAddressee(addressee) {
      return this.dialogues.find((dialogue) => {
        return dialogue.id === addressee;
      });
    },

    /**
     * Sorts dialogues by latest messages.
     *
     * @param {Dialogue} a — First dialogue.
     * @param {Dialogue} b — Second dialogue.
     * @return {number}
     */
    sortDialogues(a, b) {
      if (a.last_message.id < b.last_message.id) {
        return 1;
      }

      if (a.last_message.id > b.last_message.id) {
        return -1;
      }

      return 0;
    },

    /**
     * Checks if the dialogue has new received messages.
     *
     * @param {Dialogue} dialogue — Dialogue to check.
     * @return {boolean}
     */
    isNew(dialogue) {
      return !dialogue.last_message.read &&
        dialogue.last_message.addressee !== parseInt(this.authId);
    },

    /**
     * Checks if the dialogue has unread sent messages.
     *
     * @param {Dialogue} dialogue — Dialogue to check.
     * @return {boolean}
     */
    isUnread(dialogue) {
      return !dialogue.last_message.read &&
        dialogue.last_message.addressee === parseInt(this.authId);
    },

    /**
     * Checks if the latest message is sent by you.
     *
     * @param {Dialogue} dialogue — Dialogue to check.
     * @return {boolean}
     */
    isYours(dialogue) {
      return dialogue.last_message.addressee === parseInt(this.authId);
    }
  },

  created() {
    this.pending = true;

    // Fetching auth ID
    profileService.id()
      .then((data) => {

        // Getting authenticated user ID
        this.authId = data.data;

        // Subscribing
        this.subscribe();
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        this.pending = false;
      });

    // Fetching all messages
    profileService.myMessages()
      .then((data) => {
        this.dialogues = data.data.sort(this.sortDialogues);
      })
      .catch((err) => {
        console.error(err);
      });
  },

  beforeDestroy() {
    window.Echo.leave(`message-channel-${this.authId}`);
  },
};