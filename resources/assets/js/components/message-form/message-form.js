// Services
import profileService from '../../services/profile';

// Components
import MessageElement from '../message-element/message-element.vue';

export default {
  name: 'message-form',

  components: {MessageElement},

  props: {
    online: {
      type: String,
    }
  },

  data() {
    return {
      authId: null,
      body: '',
      bottom: 0,
      fold: false,
      messages: [],
      scroll: 0,
      scrolled: false,
      top: 0,
    };
  },

  methods: {
    /**
     * Handles submitting.
     *
     * @param {object} e — Event object.
     * @return {void}
     */
    handleSubmit(e) {
      e.preventDefault();

      const formData = new FormData();

      formData.append('destination', this.$route.params.destination);
      formData.append('body', this.body);

      profileService.sendMessage(formData)
        .then((data) => {
          this.messages = [...this.messages, data.data];
          this.body = '';
        })
        .catch((err) => console.error(err));
    },

    /**
     * Handles folding.
     *
     * @return {void}
     */
    handleFold() {
      this.fold = !this.fold;
    },

    /**
     * Handles getting back to dialogues.
     *
     * @return {void}
     */
    handleBack() {
      this.$router.go(-1);
    },

    /**
     * Scrolls dialogues down.
     *
     * @return {void}
     */
    scrollDown() {
      const el = this.$el.querySelector('.messageBlock main');

      if (el) {
        el.scrollTop = el.scrollHeight;

        this.scrolled = true;
      }
    },

    /**
     * Subscribes to channels.
     *
     * @return {void}
     */
    subscribe() {
      this.subscribeToReceive();
      this.subscribeToRead();
    },

    /**
     * Subscribes to received messages.
     * 
     * @return {void}
     */
    subscribeToReceive() {
      const channelName = `message-channel-${this.authId}`;

      // Echo channel
      const channel = window.Echo.channel(channelName);

      channel.listen('MessageSent', (data) => {
        if (data.message.addressee !== parseInt(this.$route.params.destination)) {
          return;
        }

        this.messages = [...this.messages, data.message];
      });
    },

    /**
     * Subscribes to received messages.
     * 
     * @return {void}
     */
    subscribeToRead() {
      const channelName = `message-read-${this.$route.params.destination}-${this.authId}`;

      // Echo channel
      const channel = window.Echo.channel(channelName);

      channel.listen('MessageRead', (data) => {
        let message = this.messages.find((message) => {
          return message.id === data.message.id;
        });

        const copy = [...this.messages];

        // TODO: REFACTOR
        copy[copy.indexOf(message)] = data.message;

        if (message) {
          this.messages = copy;
        }
      });
    }

  },

  /** @inheritDoc */
  created() {
    profileService.id()
      .then((data) => {
        this.authId = data.data;
      })
      .catch((err) => {
        console.error(err);
      });

    // Fetches all messages
    profileService.getUserMessages(this.$route.params.destination)
      .then((data) => {
        this.messages = data.data;
        this.subscribe();
      })
      .catch((err) => {
        console.error(err);
      });
  },

  mounted() {
    if (this.$route.name === 'profile') {
      this.fold = true;
    }

    const vm = this;
    const messageBlock = document.querySelector('.messageBlock main');

    messageBlock.addEventListener('scroll', function () {
      vm.scroll = this.scrollTop;
    });

    // Top offset
    this.top = messageBlock.getBoundingClientRect().top;

    // Bottom offset
    this.bottom = document.documentElement.offsetHeight - this.top - messageBlock.clientHeight;
  },

  watch: {
    /**
     * Fold on next tick.
     *
     * @return {void}
     */
    fold() {
      this.$nextTick(this.scrollDown);
    },

    /**
     * Watches for messages to change.
     *
     * @return {void}
     */
    messages() {
      this.$nextTick(this.scrollDown);
    },
  },

  beforeDestroy() {
    window.Echo.leave(`message-channel-${this.authId}`);
    window.Echo.leave(`message-read-${this.$route.params.destination}`);
  },
};