import profileService from '../../services/profile';

import ProfilePicture from '../profile-picture/profile-picture.vue';

export default {
  name: 'friend-searcher',

  components: {ProfilePicture},

  data() {
    return {
      active: false,
      people: [],
      query: '',
    };
  },

  methods: {
    search() {
      if (!this.query) {
        this.people = [];
        
        return;
      }

      profileService.search(this.query)
        .then((data) => {
          this.people = data.data;
        });
    }
  },

  created() {
  }
};