// Services
import bandsService from '../../services/bands';
import communitiesService from '../../services/communities';
import postsService from '../../services/posts';

export default {
  data() {
    return {
      asBand: false,
      asCommunity: false,
      authorship: '',
      title: '',
      body: '',
      bands: [],
      communities: [],
      errors: {}
    };
  },

  props: {
    bandId: {
      type: String,
    },
    communityId: {
      type: String,
    }
  },

  methods: {
    /**
     * Submits the form.
     *
     * @return void
     */
    submit() {
      const formData = new FormData();
      formData.append('as', this.asBand ? 'band' : this.asCommunity ? 'community' : ' ');
      formData.append('authorship', this.authorship);
      formData.append('body', this.body);
      formData.append('title', this.title);

      postsService.createPost(formData)
        .then((response) => this.$emit('onCreate', response.data))
        .catch((err) => this.errors = err.response.data.errors);
    },

    /**
     * Handles responses.
     *
     * @param {Response} response — Response object.
     * @return {Promise<*>}
     */
    async handleResponse(response) {
      if (!response.ok) {
        this.errors = (await response.json()).errors;
        throw Error('Something went wrong!');
      }

      return response.json();
    },

    handleBandCheckboxChange() {
      this.authorship = '';
      this.asCommunity = false;
    },

    handleCommunityCheckboxChange() {
      this.authorship = '';
      this.asBand = false;
    }
  },

  filters: {
    /**
     * Capitalizes the word.
     *
     * @param {string} word — Word as a string.
     * @returns {string}
     */
    capitalize(word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    }
  },

  /**
   * Vue hook. Fires when created.
   *
   * @returns {void}
   */
  created() {
    bandsService.getModeratedBands()
      .then(response => this.bands = response.data)
      .catch(err => console.error(err));

    communitiesService.getModeratedCommunities()
      .then(response => this.communities = response.data)
      .catch(err => console.error(err));

    if (this.bandId) {
      this.asBand = true;
      this.asCommunity = false;
      this.authorship = parseInt(this.bandId);
    }

    if (this.communityId) {
      this.asBand = false;
      this.asCommunity = true;
      this.authorship = parseInt(this.communityId);
    }
  }
};