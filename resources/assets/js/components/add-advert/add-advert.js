// Services
import advertsService from '../../services/adverts';
import bandsService from '../../services/bands';

export default {
  /**
   * Return data object.
   *
   * @return {object}
   */
  data() {
    return {
      title: '',
      body: '',
      musician: false,
      bands: [],
      asBand: false,
      errors: {}
    };
  },

  props: {
    bandId: {
      type: String,
      default: null,
    },
  },

  name: 'add-advert',

  methods: {
    /**
     * Submits the form.
     *
     * @returns void
     */
    async submit() {
      const formData = new FormData();

      formData.append('as', this.asBand);
      formData.append('body', this.body);
      formData.append('title', this.title);
      formData.append('type', this.musician ? '0' : '1');

      if (this.bandId) {
        formData.append('band', this.bandId);
      }

      try {
        const response = await advertsService.createAdvert(formData);
        this.$emit('onCreate', response.data);
      } catch (error) {
        this.errors = error.response.data.errors;
      }
    }
  },

  filters: {
    /**
     * Capitalizes the word.
     *
     * @param {string} word — Word as a string.
     * @returns {string}
     */
    capitalize(word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    }
  },

  computed: {
    isBandPage() {
      return this.$route.path !== '/adverts';
    }
  },

  /**
   * Vue hook. Fires when created.
   *
   * @returns {void}
   */
  async created() {
    try {
      const response = await bandsService.getModeratedBands();
      this.bands = response.data;
    } catch (error) {
      console.error(error);
    }
  },


  /**
   * Vue hook. Fires when mounted.
   *
   * @returns {void}
   */
  mounted() {
    this.musician = Boolean(this.bandId);
    this.asBand = Boolean(this.bandId);
  }
};
