export default {

  /**
   * Returns data object.
   *
   * @returns {{full: boolean}}
   */
  data() {
    return {
      full: false,
    };
  },

  computed: {
    /**
     * Returns image url.
     *
     * @returns {string}
     */
    urlImage() {
      return '/images/upload/' + this.path;
    },

    /**
     * Returns thumbnail url.
     *
     * @returns {string}
     */
    urlThumbnail() {
      return '/images/upload/thumbnails/' + this.path;
    }
  },

  props: {
    path: {
      required: true,
    },
    type: {
      required: true,
      type: String,
    }
  },

  methods: {
    /**
     * Toggles full screen mode.
     *
     * @returns {void}
     */
    fullPicture() {
      if (this.path && (this.type === 'full')) {
        this.full = !this.full;
      }
    },

    /**
     * Opens the image.
     *
     * @returns {void}
     */
    setPictureLink() {
      window.open(window.location.href + '/edit', '_self');
    }
  }
};
