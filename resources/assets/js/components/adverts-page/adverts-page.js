// Services
import advertsService from '../../services/adverts';

// Libs
import moment from 'moment';

// Components
import AddAdvert from '../../components/add-advert/add-advert.vue';

export default {
  /**
   * Returns an object with component data.
   *
   * @returns {object}
   */
  data() {
    return {
      adverts: [],
      filter: null,
      showForm: false
    };
  },

  components: {
    'add-advert': AddAdvert
  },

  props: {
    advertsRaw: {
      type: String,
      required: true
    },
    authId: {
      required: true,
      type: String,
    },
    bandId: {
      type: String,
    },
    editable: {
      type: String,
      required: true,
    }
  },

  /**
   * Vue hook. Fires when created.
   *
   * @returns {void}
   */
  created() {
    this.adverts = JSON.parse(this.advertsRaw);
  },

  // Methods
  methods: {
    /**
     * Sets current filter.
     *
     * @returns {void}
     */
    setFilter(type) {
      this.filter = type;
    },

    /**
     * Filters adverts.
     *
     * @returns {object[]}
     */
    filterAdverts() {
      if (this.filter === null) {
        return this.adverts;
      }

      return this.adverts.filter((ad) => {
        return +ad.type === this.filter
      });
    },

    /**
     * Deletes the advert.
     *
     * @param {number} advertId — Numeric advert ID.
     * @returns {void}
     */
    async deleteAdvert(advertId) {
      try {
        const response = await advertsService.deleteAdvert(advertId);
        this.adverts = this.adverts.filter((ad) => ad.id !== response.data);
      } catch (error) {
        console.error(err);
      }
    },

    /**
     * Handles adding new adverts.
     *
     * @param {Advert} advert
     * @returns {void}
     */
    handleCreate(advert) {
      this.showForm = false;
      this.adverts = [advert, ...this.adverts];
    }
  },

  filters: {
    /**
     * Formats the date.
     *
     * @param {string} dateString — Raw date string.
     * @returns {string}
     */
    creationDate(dateString) {
      return moment(dateString).calendar().toLowerCase();
    }
  }
};
