// Libs
import inView from 'in-view';

// Services
import profileService from '../../services/profile';

export default {
  name: 'message-element',

  data() {
    return {
      pending: false,
    };
  },

  props: {
    authId: {
      required: true,
      type: Number,
    },
    data: {
      required: true,
      type: Object,
    },
    scroll: {
      required: true,
      type: Number,
    },
    scrolled: {
      required: true,
      type: Boolean,
    },
    top: {
      required: true,
      type: Number,
    },
    bottom: {
      required: true,
      type: Number,
    }
  },

  methods: {
    /**
     * Returns an object to apply CSS classes.
     *
     * @return {object}
     */
    getClasses() {
      return {
        'messageBlock-myMessage': this.authId === this.data.addressee,
        'messageBlock-unread': !this.data.read
      };
    },

    /**
     * Checks if the message is read.
     *
     * @return {void}
     */
    checkIfRead() {
      if (this.pending) {
        return;
      }

      if (this.checkIfToRead()) {
        this.pending = true;

        profileService.readMessage(this.data.id)
          .then((data) => {
            if (data.data) {
              setTimeout(() => this.data.read = true, 300);
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    },

    /**
     * Checks if the message is to be read.
     *
     * @returns {boolean}
     */
    checkIfToRead() {
      return inView.is(this.$el) &&
        !this.data.read &&
        this.scrolled &&
        this.data.destination === this.authId;
    }
  },

  watch: {
    /**
     * Watches for the "scroll" prop to change.
     *
     * @return {void}
     */
    scroll() {
      this.checkIfRead();
    },

    /**
     * Watches for the "scrolled" prop to change.
     *
     * @return {void}
     */
    scrolled() {
      this.checkIfRead();
    }
  },

  created() {
    inView.offset({
      top: this.top,
      bottom: this.bottom,
    });
  },
};