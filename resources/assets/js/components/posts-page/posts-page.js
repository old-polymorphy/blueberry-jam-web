// Services
import postsService from '../../services/posts';

// Libs
import moment from 'moment';

// Components
import AddPost from '../../components/add-post/add-post.vue';

export default {
  /**
   * Returns an object with component data.
   *
   * @returns {object}
   */
  data() {
    return {
      posts: [],
      showForm: false
    };
  },

  components: {
    'add-post': AddPost
  },

  props: {
    authId: {
      type: String,
      required: true,
    },
    bandId: {
      type: String,
    },
    communityId: {
      type: String,
    },
    editable: {
      type: String,
      required: true,
    },
    postsRaw: {
      type: String,
    },
    userId: {
      type: String,
    }
  },

  /**
   * Vue hook. Fires when created.
   *
   * @returns {void}
   */
  created() {
    this.posts = JSON.parse(this.postsRaw);
  },

  // Methods
  methods: {
    /**
     * Deletes the post.
     *
     * @param {number} postId — Numeric post ID.
     * @returns {void}
     */
    deletePost(postId) {
      postsService.deletePost(postId)
        .then((response) => {
          this.posts = this.posts
            .filter((post) => post.id !== response.data);
        })
        .catch((err) => console.error(err));
    },

    /**
     * Handles adding new adverts.
     *
     * @param {Post} post
     * @returns {void}
     */
    handleCreate(post) {
      this.showForm = false;
      this.posts = [post, ...this.posts];
    }
  },

  filters: {
    /**
     * Formats the date.
     *
     * @param {string} dateString — Raw date string.
     * @returns {string}
     */
    creationDate(dateString) {
      return moment(dateString).calendar().toLowerCase();
    }
  }
};