// Services
import profileService from '../../services/profile';

export default {
  name: 'news-counter',

  props: {
    type: {
      required: true,
      type: String,
    }
  },

  data() {
    return {
      counter: 0,
    };
  },

  created() {
    profileService.notifications('messages')
      .then((data) => {
        this.counter = data.data;
      });
  }
};