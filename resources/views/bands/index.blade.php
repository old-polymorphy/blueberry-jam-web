@extends('layouts.app')

@section('title')
    {{ $band->name }}
@endsection

@section('content')

    {{-- Profile header --}}
    <div class="details profile-details">

        <profile-picture path="{{ $band->picture }}" type="full"></profile-picture>


        {{-- User information --}}
        <div class="profile-inf">

            {{-- Name --}}
            <h1>{{ $band->name }}</h1>

            {{-- Edit button --}}
            @if (isAdmin($band))
                <a class="gray-link" href="{{ request()->url() . '/edit' }}">edit</a>
            @endif

            {{-- Location --}}
            <div>{{ displayLocation($band->city, $band->country) }}</div>

            {{-- Subscription --}}
            @if ($band->users->contains(auth()->id()))
                <a href="{{ request()->url() . '/unsubscribe' }}">Unsubscribe</a>
            @else
                <a href="{{ request()->url() . '/subscribe' }}">Subscribe</a>
            @endif

        </div>

        {{-- Clear --}}
        <div class="clear"></div>
    </div>

    {{-- Buttons --}}
    <nav class="profile-navigation">
        <button class="active">Info</button>
        <button>Adverts</button>
        <button>Posts</button>
    </nav>

    {{-- Info wrapper --}}
    <div class="profile-tab profile-info-tab">

        {{-- Bio --}}
        @if ($band->bio)
            <h2>Bio</h2>

            {{-- Edit button --}}
            @if (isAdmin($band))
                <a class="gray-link" href="{{ request()->url() . '/edit' }}">edit</a>
            @endif

            <section>
                {{ $band->bio }}
            </section>
        @endif

        {{-- Users --}}
        @if (! $users->isEmpty())
            <h2>Subscribers ({{ count($users) }})</h2>

            <a class="gray-link" href="{{ request()->url() . '/subscribers' }}">see all</a>

            <section class="horizontalList">
                @foreach ($users as $user)
                    <a class="horizontalList-element" href="{{ url('/') }}/profile/{{ $user->id }}">
                        <profile-picture path="{{ $user->picture }}" type="list"></profile-picture>
                        <span>{{ $user->name }}</span>
                    </a>
                @endforeach
            </section>
        @endif

    </div>

    {{-- Adverts wrapper --}}
    <div class="profile-tab profile-adverts-tab hidden">

        {{-- Adverts --}}
        <adverts-page
                adverts-raw="{{ $adverts }}"
                auth-id="{{auth()->id()}}"
                editable="{{ $editable }}"
                band-id="{{ $band->id }}"
        ></adverts-page>

    </div>

    {{-- Posts wrapper --}}
    <div class="profile-tab profile-posts-tab hidden">

        {{-- Posts --}}
        <posts-page
                auth-id="{{ auth()->id() }}"
                band-id="{{ $band->id }}"
                editable="{{ $editable }}"
                posts-raw="{{ $posts }}"
        ></posts-page>

    </div>

@endsection
