{{-- Parent layot --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', "Subscribers of {$band->name}")

{{-- Content --}}
@section('content')

    <div class="pageWrapper">

        {{-- Back link --}}
        <h1><a class="backLink" href="{{ "/band/{$band->id}" }}">{{ $band->name }}</a></h1>

        {{-- Buttons --}}
        <nav class="profile-navigation">
            <button class="active">Users</button>
            <button>Moderators</button>
            <button>Administrators</button>
        </nav>

        {{-- Users --}}
        <div class="profile-tab profile-users-tab">
            @if (count($users))
                <div class="verticalList">
                    @foreach ($users as $user)
                        @include ('layouts.users')
                    @endforeach
                </div>
            @else
                The band has no subscribers yet.
            @endif
        </div>

        {{-- Moderators --}}
        <div class="profile-tab profile-moderators-tab hidden">
            @if (count($moderators))
                <div class="verticalList">
                    @foreach ($moderators as $user)
                        @include ('layouts.users')
                    @endforeach
                </div>
            @else
                The band has no moderators.
            @endif
        </div>

        {{-- Administrators --}}
        <div class="profile-tab profile-administrators-tab hidden">
            <div class="verticalList">
                @foreach ($admins as $user)
                    @include ('layouts.users')
                @endforeach
            </div>
        </div>

    </div>

@endsection
