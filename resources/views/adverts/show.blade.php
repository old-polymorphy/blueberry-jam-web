@extends('layouts.app')

@section('title')
    {{ $advert->title }}
@endsection

@section('content')
    <div class="details">
        <h1>{{ $advert->title }}</h1>

        <p>
            <span class="tag">{{ ($advert->type) ? 'band' : 'musician'}}</span>
        </p>

        <p>Author: <a href="/profile/{{ $advert->user_id }}">{{ nameById($advert->user_id) }}</a></p>

        <p>Published: at {{ $advert->created_at->toTimeString() }}
            on {{ $advert->created_at->toFormattedDateString() }}</p>

        <p>
            {{ $advert->body }}
        </p>
    </div>
@endsection
