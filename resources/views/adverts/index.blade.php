{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', 'Adverts')

{{-- Content --}}
@section('content')

    {{-- Adverts --}}
    <div class="pageWrapper">

        {{-- Heading --}}
        <h1>Adverts</h1>

        {{-- Adverts --}}
        <adverts-page
            adverts-raw="{{ $adverts }}"
            auth-id="{{ auth()->id() }}"
            editable="{{ auth()->id() }}"
        ></adverts-page>

    </div>

@endsection
