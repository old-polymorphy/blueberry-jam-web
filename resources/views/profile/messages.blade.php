{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', "Messages of {$user->name}")

{{-- Content --}}
@section('content')
    <div class="messagePage dialogueList pageWrapper">

        {{-- Message page --}}
        <router-view></router-view>

    </div>
@endsection
