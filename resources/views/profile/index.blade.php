{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', $user->name)

{{-- Content --}}
@section('content')

    <div class="profile">

        {{-- Profile header --}}
        <div class="details profile-details">

            <profile-picture path="{{ $user->picture }}" type="full"></profile-picture>

            {{-- User information --}}
            <div class="profile-inf">

                {{-- Name --}}
                <h1>{{ displayName($user->first_name, $user->last_name) ?? $user->name }}</h1>

                {{-- Edit button --}}
                @if ($user->id === auth()->id())
                    <a class="gray-link" href="{{ request()->url() . '/edit' }}">edit</a>
                @endif

                {{-- Location --}}
                <div>{{ displayLocation($user->city, $user->country) }}</div>

                {{-- Online --}}
                <div class="{{ $user->online() ? 'online' : 'offline'}}">{{ $user->online() ? 'online' : 'offline' }}</div>

            </div>

        </div>

        {{-- Buttons --}}
        <nav class="profile-navigation">
            <button class="active">Info</button>
            <button>Adverts</button>
            <button>Posts</button>
        </nav>

        {{-- Info wrapper --}}
        <div class="profile-tab profile-info-tab">

            {{-- Bio --}}
            @if ($user->bio)
                <h2>Bio</h2>

                @if ($user->id === auth()->id())
                    <a class="gray-link" href="{{ request()->url() . '/edit' }}">edit</a>
                @endif

                <section>
                    {{ $user->bio }}
                </section>
            @endif

            {{-- Instruments --}}
            @if (! $user->instruments()->pluck('name')->isEmpty())
                <h2>Instruments</h2>

                @if ($user->id === auth()->id())
                    <a class="gray-link" href="{{ request()->url() . '/edit' }}">edit</a>
                @endif

                <section>
                    @foreach ($user->instruments()->pluck('name') as $instrument)
                        <span class="tag">{{ $instrument }}</span>
                    @endforeach
                </section>
            @endif

            {{-- Genres --}}
            @if (! $user->genres()->pluck('name')->isEmpty())
                <h2>Genres</h2>

                @if ($user->id === auth()->id())
                    <a class="gray-link" href="{{ request()->url() . '/edit' }}">edit</a>
                @endif
                <section>
                    @foreach ($user->genres()->pluck('name') as $genre)
                        <span class="tag">{{ $genre }}</span>
                    @endforeach
                </section>
            @endif

            {{-- Friends --}}
            @if (! $friends->isEmpty())
                <h2>Friends</h2>

                <a class="gray-link" href="{{ request()->url() . '/friends' }}">see all</a>

                <section class="horizontalList">
                    @foreach ($friends as $friend)
                        <a class="horizontalList-element" href="{{ url('/') }}/profile/{{ $friend->id }}">
                            <profile-picture path="{{ $friend->picture }}" type="list"></profile-picture>
                            <span>{{ $friend->name }}</span>
                        </a>
                    @endforeach
                </section>
            @endif

            {{-- Bands --}}
            @if (!$bands->isEmpty())
                <h2>Bands</h2>

                <a class="gray-link" href="{{ request()->url() . '/bands' }}">see all</a>

                <section class="horizontalList">
                    @foreach ($bands as $band)
                        <a class="horizontalList-element" href="{{ url('/') }}/band/{{ $band->id }}">
                            <profile-picture path="{{ $band->picture }}" type="list"></profile-picture>
                            <span>{{ $band->name }}</span>
                        </a>
                    @endforeach
                </section>
            @endif

            {{-- Communities --}}
            @if (!$communities->isEmpty())
                <h2>Communities</h2>

                <a class="gray-link" href="{{ request()->url() . '/communities' }}">see all</a>

                <section class="horizontalList">
                    @foreach ($communities as $community)
                        <a class="horizontalList-element" href="{{ url('/') }}/community/{{ $community->id }}">
                            <profile-picture path="{{ $community->picture }}" type="list"></profile-picture>
                            <span>{{ $community->name }}</span>
                        </a>
                    @endforeach
                </section>
            @endif

        </div>

        {{-- Adverts wrapper --}}
        <div class="profile-tab profile-adverts-tab hidden">

            {{-- Adverts --}}
            <adverts-page
                    adverts-raw="{{ $adverts }}"
                    auth-id="{{ auth()->id() }}"
                    editable="{{ $editable }}"
            ></adverts-page>

        </div>

        {{-- Posts wrapper --}}
        <div class="profile-tab profile-posts-tab hidden">

            {{-- Posts --}}
            <posts-page
                    auth-id="{{ auth()->id() }}"
                    editable="{{ $editable }}"
                    posts-raw="{{ $posts }}"
                    user-id="{{ $user->id }}"
            ></posts-page>

        </div>

        {{-- Add friend button --}}
        @if (auth()->id() && auth()->id() !== $user->id)
            <form action="/profile/{{ $user->id }}/add" class="profile-friendForm" method="POST">
                {{ csrf_field() }}
                @if (!$user->subscribers->contains(auth()->id()))
                    <a class="addFriend addButton" title="Add Friend"></a>
                @else
                    @if (!$user->requests->contains(auth()->id()))
                        <a class="cancelFriend addButton" title="Cancel Request"></a>
                    @else
                        <a class="deleteFriend addButton" title="Delete Friend"></a>
                    @endif
                @endif
            </form>
        @endif

        {{-- Message block --}}
        @if (auth()->id() && $user->id !== auth()->id())
            <router-view></router-view>
        @endif

    </div>

@endsection
