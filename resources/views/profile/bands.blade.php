{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', "Bands of {$user->name}")

{{-- Content --}}
@section('content')

    <div class="pageWrapper">

        {{-- Back link --}}
        <h1><a class="backLink" href="{{ "/profile/{$user->id}" }}">{{ $user->name }}</a></h1>

        {{-- Buttons --}}
        <nav class="profile-navigation">
            <button class="active">Search</button>
            <button>Follower</button>
            <button>Moderator</button>
            <button>Administrator</button>
            <button>Creator</button>
        </nav>

        {{-- Search --}}
        <div class="profile-tab profile-search-tab">

            <h2>Top 10 Bands</h2>

            <div class="verticalList">
                @foreach ($trending as $band)
                    @include ('layouts.bands')
                @endforeach
            </div>

        </div>

        {{-- Follower --}}
        <div class="profile-tab profile-follower-tab hidden">
            <div class="verticalList">
                @foreach ($bands as $band)
                    @include ('layouts.bands')
                @endforeach
            </div>
        </div>

        {{-- Moderator --}}
        <div class="profile-tab profile-moderator-tab hidden">
            <div class="verticalList">
                @foreach ($moderator as $band)
                    @include ('layouts.bands')
                @endforeach
            </div>
        </div>

        {{-- Administrator --}}
        <div class="profile-tab profile-administrator-tab hidden">
            <div class="verticalList">
                @foreach ($administrator as $band)
                    @include ('layouts.bands')
                @endforeach
            </div>
        </div>

        {{-- Creator --}}
        <div class="profile-tab profile-creator-tab hidden">
            <div class="verticalList">
                @foreach ($creator as $band)
                    @include ('layouts.bands')
                @endforeach
            </div>
        </div>

    </div>

@endsection
