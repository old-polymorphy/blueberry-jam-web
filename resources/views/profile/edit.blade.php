{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', 'Edit Profile')

{{-- Content --}}
@section('content')
    <div class="pageWrapper">
        <form action="/profile/{{ $user->id }}" class="editForm form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <h1>Edit your profile</h1>

            {{-- Information --}}
            <h2>Info</h2>
            <section class="editForm-info">

                {{-- Name --}}
                <div class="form-group{{ $errors->has('name') ? ' alert' : '' }}">
                    <label for="username">Username</label>
                    <input id="username" name="name" type="text"
                           value="{{ $errors->has('name') ? old('name') : $user->name }}">

                    {{-- Error block --}}
                    @if ($errors->has('name'))
                        <ul class="errorList">
                            @foreach ($errors->get('name') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                </div>

                {{-- First name --}}
                <div class="form-group{{ $errors->has('first_name') ? ' alert' : '' }}">
                    <label for="username">First name</label>
                    <input id="username" name="first_name" type="text"
                           value="{{ $errors->has('first_name') ? old('first_name') : $user->first_name }}">

                    {{-- Error block --}}
                    @if ($errors->has('first_name'))
                        <ul class="errorList">
                            @foreach ($errors->get('first_name') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                </div>

                {{-- Last name --}}
                <div class="form-group{{ $errors->has('last_name') ? ' alert' : '' }}">
                    <label for="username">Last name</label>
                    <input id="username" name="last_name" type="text"
                           value="{{ $errors->has('last_name') ? old('last_name') : $user->last_name }}">

                    {{-- Error block --}}
                    @if ($errors->has('last_name'))
                        <ul class="errorList">
                            @foreach ($errors->get('last_name') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                </div>

                {{-- City --}}
                <div class="form-group">
                    <label for="username">City</label>
                    <input id="username" name="city" type="text"
                           value="{{ $errors->has('city') ? old('city') : $user->city }}">
                </div>

                {{-- Country --}}
                <div class="form-group">
                    <label for="username">Country</label>
                    <input id="username" name="country" type="text"
                           value="{{ $errors->has('country') ? old('country') : $user->country }}">
                </div>

                {{-- Bio --}}
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea id="bio" name="bio"
                              rows="10">{{ $errors->has('bio') ? old('bio') : $user->bio }}</textarea>
                </div>

            </section>

            {{-- Picture --}}
            <h2>Picture</h2>
            <section>

                {{-- Set picture --}}
                <div class="form-group">

                    {{-- Custom file field --}}
                    <div class="custom-file button customFile">
                        <span class="customFile-text">Choose File</span>
                        <input class="customFile-input" name="picture" type="file">
                    </div>

                    {{-- Delete picture button --}}
                    <div class="button delete-button editForm-deletePictureButton">Clear Picture</div>

                    {{-- Image errors --}}
                    @if ($errors->has('picture'))
                        <ul class="errorList">
                            @foreach ($errors->get('picture') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                </div>
            </section>

            {{-- Instruments --}}
            <h2>Instruments</h2>

            {{-- Wrapper --}}
            <section class="editForm-checkboxes">

                {{-- Loop --}}
                @foreach($instruments as $instrument)
                    <div class="customCheckbox">

                        {{-- Input --}}
                        <input class="customCheckbox-input"
                               {{ $user->instruments->contains($instrument->id) ? 'checked' : '' }}
                               name="{{ $instrument->name }}"
                               id="editForm-{{ $instrument->name }}Instrument"
                               type="checkbox">

                        {{-- Label --}}
                        <label class="customCheckbox-label"
                               for="editForm-{{ $instrument->name }}Instrument">
                            {{ $instrument->name }}
                        </label>

                    </div>
                @endforeach

            </section>

            {{-- Genres --}}
            <h2>Genres</h2>

            {{-- Wrapper --}}
            <section class="editForm-checkboxes">

                {{-- Loop --}}
                @foreach($genres as $genre)
                    <div class="customCheckbox">

                        {{-- Input --}}
                        <input class="customCheckbox-input"
                               {{ $user->genres->contains($genre->id) ? 'checked' : '' }}
                               name="{{ $genre->name }}"
                               id="editForm-{{ $genre->name }}Instrument"
                               type="checkbox">

                        {{-- Label --}}
                        <label class="customCheckbox-label"
                               for="editForm-{{ $genre->name }}Instrument">
                            {{ $genre->name }}
                        </label>

                    </div>
                @endforeach

            </section>

            {{-- Submit --}}
            <button class="button" type="submit">Save</button>

        </form>
    </div>

@endsection
