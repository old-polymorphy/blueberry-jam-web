{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', "Communities of {$user->name}")

{{-- Content --}}
@section('content')

    <div class="pageWrapper">

        {{-- Back link --}}
        <h1><a class="backLink" href="{{ "/profile/{$user->id}" }}">{{ $user->name }}</a></h1>

        {{-- Buttons --}}
        <nav class="profile-navigation">
            <button class="active">Follower</button>
            <button>Moderator</button>
            <button>Administrator</button>
            <button>Creator</button>
        </nav>

        {{-- Follower --}}
        <div class="profile-tab profile-follower-tab">
            <div class="verticalList">
                @foreach ($communities as $community)
                    @include ('layouts.communities')
                @endforeach
            </div>
        </div>

        {{-- Moderator --}}
        <div class="profile-tab profile-moderator-tab hidden">
            <div class="verticalList">
                @foreach ($moderator as $community)
                    @include ('layouts.communities')
                @endforeach
            </div>
        </div>

        {{-- Administrator --}}
        <div class="profile-tab profile-administrator-tab hidden">
            <div class="verticalList">
                @foreach ($administrator as $community)
                    @include ('layouts.communities')
                @endforeach
            </div>
        </div>

        {{-- Creator --}}
        <div class="profile-tab profile-creator-tab hidden">
            <div class="verticalList">
                @foreach ($creator as $community)
                    @include ('layouts.communities')
                @endforeach
            </div>
        </div>

    </div>

@endsection
