{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', "Friends of {$user->name}")

{{-- Content --}}
@section('content')

    <div class="pageWrapper">

        {{-- Back link --}}
        <h1><a class="backLink" href="{{ "/profile/{$user->id}" }}">{{ $user->name }}</a></h1>

        <friend-searcher></friend-searcher>

        {{-- Buttons --}}
        <nav class="profile-navigation">
            <button class="active">Friends</button>
            <button>Subscribers</button>
            <button>Requests</button>
        </nav>

        {{-- Actual friends --}}
        <div class="profile-tab profile-friends-tab">
            @if (count($friends))
                <div class="verticalList">
                    @foreach ($friends as $user)
                        @include ('layouts.users')
                    @endforeach
                </div>
            @else
                You don't have any friends just yet.
            @endif
        </div>

        {{-- Subscribers --}}
        <div class="profile-tab profile-subscribers-tab hidden">
            @if (count($subscribers))
                <div class="verticalList">
                    @foreach ($subscribers as $user)
                        @include ('layouts.users')
                    @endforeach
                </div>
            @else
                You don't have any subscribers or incoming requests.
            @endif
        </div>

        {{-- Friend requests --}}
        <div class="profile-tab profile-requests-tab hidden">
            @if (count($requests))
                <div class="verticalList">
                    @foreach ($requests as $user)
                        @include ('layouts.users')
                    @endforeach
                </div>
            @else
                You don't have any outgoing requests.
            @endif
        </div>

    </div>

@endsection
<script>
  import FriendSearcher from '../../assets/js/components/friend-searcher/friend-searcher';

  export default {
    components: {FriendSearcher}
  };
</script>