{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', 'Edit Profile')

{{-- Content --}}
@section('content')

    <div class="pageWrapper">
        <form action="/community/{{ $community->id }}" class="editForm form" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <h1>Edit your community</h1>

            {{-- Picture --}}
            <h2>Picture</h2>
            <section>

                {{-- Set picture --}}
                <div class="form-group">

                    {{-- Custom file field --}}
                    <div class="custom-file button customFile">
                        <span class="customFile-text">Choose File</span>
                        <input class="customFile-input" name="picture" type="file">
                    </div>

                    {{-- Delete picture button --}}
                    <div class="button delete-button editForm-deletePictureButton">Clear Picture</div>

                    {{-- Image errors --}}
                    @if ($errors->has('picture'))
                        <ul class="errorList">
                            @foreach ($errors->get('picture') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                </div>
            </section>

            {{-- Information --}}
            <h2>Info</h2>
            <section class="editForm-info">

                {{-- Name --}}
                <div class="form-group{{ $errors->has('name') ? ' alert' : '' }}">
                    <label for="username">Name</label>
                    <input id="username" name="name" type="text"
                           value="{{ $errors->has('name') ? old('name') : $community->name }}">
                    @if ($errors->has('name'))
                        <ul class="errorList">
                            @foreach ($errors->get('name') as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>

                {{-- City --}}
                <div class="form-group">
                    <label for="city">City</label>
                    <input id="city" name="city" type="text"
                           value="{{ $errors->has('city') ? old('city') : $community->city }}">
                </div>

                {{-- Country --}}
                <div class="form-group">
                    <label for="country">Country</label>
                    <input id="country" name="country" type="text"
                           value="{{ $errors->has('country') ? old('country') : $community->country }}">
                </div>

                {{-- Bio --}}
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea id="bio" name="bio"
                              rows="10">{{ $errors->has('bio') ? old('bio') : $community->bio }}</textarea>
                </div>

            </section>

            {{-- Submit --}}
            <button class="button" type="submit">Save</button>

        </form>

        {{-- Delete community --}}
        <form action="{{'delete'}}" class="editForm form" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            {{-- Delete --}}
            <h2>Delete Community</h2>
            <section>
                <button class="button delete-button">Delete</button>
            </section>
        </form>

    </div>

@endsection
