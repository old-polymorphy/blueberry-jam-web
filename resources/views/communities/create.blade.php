@extends('layouts.app')

@section('title', 'Create Community')

@section('content')

    <form action="{{'/community'}}" class="add form" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

        <h1>Create a community</h1>

        <section>

            {{-- Name --}}
            <div class="form-group{{ $errors->has('name') ? ' alert' : '' }}">
                <input aria-label="Name" name="name" placeholder="Name"
                       value="{{ $errors->any() ? old('name') : '' }}" required>
            </div>
            @if ($errors->has('name'))
                <ul class="errorList">
                    @foreach ($errors->get('name') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {{-- Bio --}}
            <div class="form-group{{ $errors->has('bio') ? ' alert' : '' }}">
            <textarea aria-label="Bio" name="bio" placeholder="Your community's bio..."
                      rows="10" required>{{ $errors->any() ? old('bio') : '' }}</textarea>
            </div>
            @if ($errors->has('bio'))
                <ul class="errorList">
                    @foreach ($errors->get('bio') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif


        </section>

        {{-- Picture --}}
        <section>

            {{-- Set picture --}}
            <div class="form-group">

                {{-- Custom file field --}}
                <div class="button customFile">
                    <span class="customFile-text">Choose Picture</span>
                    <input class="customFile-input" name="picture" type="file" value="1">
                </div>

                {{-- Image errors --}}
                @if ($errors->has('picture'))
                    <ul class="errorList">
                        @foreach ($errors->get('picture') as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

            </div>
        </section>

        {{-- Submit --}}
        <div class="form-group">
            <button class="button" type="submit">Create</button>
        </div>

    </form>
@endsection
