@extends('layouts.app')

@section('title')
    {{ $community->name }}
@endsection

@section('content')

    {{-- Profile header --}}
    <div class="details profile-details">

        {{-- Profile picture --}}
        <profile-picture path="{{ $community->picture }}" type="full"></profile-picture>

        {{-- User information --}}
        <div class="profile-inf">

            {{-- Name --}}
            <h1>{{ $community->name }}</h1>

            {{-- Edit button --}}
            @if (isAdmin($community))
                <a class="gray-link" href="{{ request()->url() . '/edit' }}">edit</a>
            @endif

            {{-- Location --}}
            <div>{{ displayLocation($community->city, $community->country) }}</div>

            {{-- Subscription --}}
            @if ($community->users->contains(auth()->id()))
                <a href="{{ request()->url() . '/unsubscribe' }}">Unsubscribe</a>
            @else
                <a href="{{ request()->url() . '/subscribe' }}">Subscribe</a>
            @endif

        </div>

        {{-- Clear --}}
        <div class="clear"></div>
    </div>

    {{-- Buttons --}}
    <nav class="profile-navigation">
        <button class="active">Info</button>
        <button>Posts</button>
    </nav>

    {{-- Info wrapper --}}
    <div class="profile-tab profile-info-tab">

        {{-- Bio --}}
        @if ($community->bio)
            <h2>Bio</h2>

            {{-- Edit button --}}
            @if (isAdmin($community))
                <a class="gray-link" href="{{ request()->url() . '/edit' }}">edit</a>
            @endif

            <section>
                {{ $community->bio }}
            </section>
        @endif

        {{-- Users --}}
        @if (! $users->isEmpty())
            <h2>Subscribers ({{ count($users) }})</h2>

            <a class="gray-link" href="{{ request()->url() . '/subscribers' }}">see all</a>

            <section class="horizontalList">
                @foreach ($users as $user)
                    <a class="horizontalList-element" href="{{ url('/') }}/profile/{{ $user->id }}">
                        <profile-picture path="{{ $user->picture }}" type="list"></profile-picture>
                        <span>{{ $user->name }}</span>
                    </a>
                @endforeach
            </section>
        @endif

    </div>

    {{-- Posts wrapper --}}
    <div class="profile-tab profile-posts-tab hidden">

        {{-- Posts --}}
        <posts-page
                auth-id="{{ auth()->id() }}"
                community-id="{{ $community->id }}"
                editable="{{ $editable }}"
                posts-raw="{{ $posts }}"
        ></posts-page>

    </div>

@endsection
