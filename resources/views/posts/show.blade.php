{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', $post->title)

{{-- Content --}}
@section('content')
    <div class="details">
        <h1>{{ $post->title }}</h1>

        <p>Author: <a href="/profile/{{ $post->user_id }}">{{ nameById($post->user_id) }}</a></p>

        <p>Published: at {{ $post->created_at->toTimeString() }} on {{ $post->created_at->toFormattedDateString() }}</p>

        <p>{{ $post->body }}</p>
    </div>
@endsection
