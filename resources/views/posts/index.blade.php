{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', 'Feed')

{{-- Content --}}
@section('content')

    {{-- Adverts --}}
    <div class="pageWrapper">
        <h1>Posts</h1>
        <posts-page
                auth-id="{{ auth()->id() }}"
                editable="{{ auth()->id() }}"
                posts-raw="{{ $posts }}"
        ></posts-page>
    </div>

@endsection
