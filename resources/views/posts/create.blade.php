{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', 'Create Post')

{{-- Content --}}
@section('content')
    <add-post></add-post>
@endsection
