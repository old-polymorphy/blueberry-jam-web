<div class="details profile-details listOfProfiles verticalList-element">

    {{-- Profile picture --}}
    <a href="{{ url('/') }}/profile/{{ $user->id }}" class="verticalList-elementPicture">
        <profile-picture path="{{ $user->picture }}" type="list"></profile-picture>
    </a>

    {{-- User information --}}
    <div class="profile-inf">

        {{-- Name --}}
        <a href="{{ url('/') }}/profile/{{ $user->id }}">
            <h2 class="verticalList-elementHeading">
                {{ displayName($user->first_name, $user->last_name) ?? $user->name  }}
            </h2>
        </a>

        {{-- Location --}}
        <div>{{ displayLocation($user->city, $user->country) }}</div>

        {{-- Online --}}
        <div class="{{ $user->online() ? 'online' : 'offline'}}">{{ $user->online() ? 'online' : 'offline' }}</div>

    </div>

    {{-- Clear --}}
    <div class="clear"></div>
</div>