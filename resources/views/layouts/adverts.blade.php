{{-- Displaying adverts --}}
@foreach ($adverts as $advert)
    <div class="listElement">

        {{-- Delete button --}}
        @if ($advert->user_id === auth()->id())
            <form action="/advert/{{ $advert->id }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <button class="listElement-deleteButton"></button>
            </form>
        @endif

        {{-- Title --}}
        <h2 class="listElement-heading">
            <a href="/advert/{{ $advert->id }}">{{ $advert->title }}</a>
        </h2>

        {{-- Type --}}
        <p>
            <span class="tag">{{ ($advert->type) ? 'band' : 'musician'}}</span>
        </p>

        {{-- Body --}}
        <p>{{ $advert->body }}</p>

        {{-- Bottom --}}
        <div>Published by
            @if ($advert->band_id)
                <a href="/band/{{ $advert->band_id }}">{{ $advert->band()->name }}</a>
                {{ $advert->created_at->diffForHumans() }}
            @else
                <a href="/profile/{{ $advert->user_id }}">{{ nameById($advert->user_id) }}</a>
                {{ $advert->created_at->diffForHumans() }}
            @endif
        </div>
    </div>
@endforeach