<!doctype html>

<html lang="en">
<head>

    {{-- Title --}}
    <title>@yield('title')</title>

    {{-- CSS --}}
    <link rel="stylesheet" href="{{'/css/app.css'}}">

    {{-- CSRF token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Yandex.Webmaster --}}
    <meta name="yandex-verification" content="8cfa322b83ba01b2"/>

    {{-- Viewport --}}
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>

<div id="app">
    {{-- Left navigation --}}
    <nav class="leftNavigation">
        @include('layouts.sidebar')
    </nav>

    {{-- Main --}}
    <main class="content">
        <div class="wrapper">
            @yield('content')
        </div>
    </main>
</div>

{{-- Navigation hamburger --}}
<button class="hamburger hamburger--arrow navHamburger" type="button">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
</button>

{{-- Session message --}}
@if (session('message'))
    <div class="flash">
        {{ session('message') }}
    </div>
@endif

{{-- Statistics --}}
@if (app()->environment('production'))
    @include('statistics.yanndex-metrika')
@endif

{{-- Scripts --}}
<script src="{{'/js/app.js'}}"></script>

</body>
</html>
