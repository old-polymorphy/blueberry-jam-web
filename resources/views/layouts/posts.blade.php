{{-- Displaying adverts --}}
@foreach ($posts as $post)
    <div class="listElement">

        @if ($post->user_id === auth()->id())
            <form action="/post/{{ $post->id }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <button class="listElement-deleteButton"></button>
            </form>
        @endif

        <h2 class="listElement-heading">
            <a href="/post/{{ $post->id }}">{{ $post->title }}</a>
        </h2>

        {{-- Body --}}
        <p>
            {{ $post->body }}
        </p>

        {{-- Bottom --}}
        <div>Published by
            @if ($post->band_id)
                <a href="/band/{{ $post->band_id }}">{{ $post->band()->name }}</a>
                {{ $post->created_at->diffForHumans() }}
            @else
                <a href="/profile/{{ $post->user_id }}">{{ nameById($post->user_id) }}</a>
                {{ $post->created_at->diffForHumans() }}
            @endif
        </div>
    </div>
@endforeach