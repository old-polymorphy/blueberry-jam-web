{{-- Common --}}
<a class="adverts{{ isActive('adverts') }}" href="{{ url('/adverts')}}">
    <span>Adverts</span>
</a>

{{-- Optional items --}}
@if (auth()->check())

    {{-- Feed --}}
    <a class="feed{{ isActive('posts') }}" href="{{ url('/posts') }}">
        <span>Feed</span>
    </a>

    {{-- Profile --}}
    <a class="profile{{ isActive('profile') }}" href="{{ url('/profile') }}/{{ auth()->id() }}">
        <span>Profile</span>
    </a>

    {{-- Friends --}}
    <a class="friends{{ isActive('friends') }}" href="{{ url('/profile') }}/{{ auth()->id() }}/friends">
        <span>Friends</span>
    </a>

    {{-- Communities --}}
    <a class="communities{{ isActive('community') }}" href="/profile/{{ auth()->id() }}/communities">
        <span>Communities</span>
    </a>

    {{-- Bands --}}
    <a class="bands{{ isActive('band') }}" href="/profile/{{ auth()->id() }}/bands">
        <span>Bands</span>
    </a>

    {{-- Messages --}}
    <a class="bookmarks{{ isActive('messages') }}" href="{{ '/messages' }}">

        {{-- Text --}}
        <span>Messages</span>

        {{-- Counter --}}
        <news-counter type="messages"></news-counter>

    </a>

    {{-- Log Out --}}
    <a class="logout" href="{{ url('/logout') }}">
        <span>Sign Out</span>
    </a>

@else
    {{-- Log In --}}
    <a class="login{{ isActive('login') }}" href="{{ url('/login') }}">
        <span>Sign In</span>
    </a>

    {{-- Sign Up --}}
    <a class="register{{ isActive('register') }}" href="{{ url('/register') }}">
        <span>Sign Up</span>
    </a>
@endif
