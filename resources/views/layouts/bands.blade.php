{{-- Profile header --}}
<div class="details profile-details listOfProfiles verticalList-element">

    {{-- Profile picture --}}
    <a href="{{ url('/') }}/band/{{ $band->id }}" class="verticalList-elementPicture">
        <profile-picture path="{{ $band->picture }}" type="list"></profile-picture>
    </a>

    {{-- User information --}}
    <div class="profile-inf">

        {{-- Name --}}
        <a href="{{ url('/') }}/band/{{ $band->id }}">
            <h2 class="verticalList-elementHeading">{{ $band->name }}</h2>
        </a>

        {{-- Location --}}
        <div>{{ displayLocation($band->city, $band->country) }}</div>

        {{-- Subscription --}}
        @if ($band->users->contains(auth()->id()))
            <a href="{{ url('/') }}/band/{{ $band->id }}/unsubscribe">Unsubscribe</a>
        @else
            <a href="{{ url('/') }}/band/{{ $band->id }}/subscribe">Subscribe</a>
        @endif

    </div>

    {{-- Clear --}}
    <div class="clear"></div>

    {{-- Add advert button --}}
    @if (auth()->id())
        <a class="addBand addButton" href="{{ '/bands/create' }}"></a>
    @endif

</div>