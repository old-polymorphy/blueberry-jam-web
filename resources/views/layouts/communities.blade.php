{{-- Profile header --}}
<div class="details profile-details listOfProfiles verticalList-element">

    {{-- Profile picture --}}
    <a href="{{ url('/') }}/community/{{ $community->id }}" class="verticalList-elementPicture">
        <profile-picture path="{{ $community->picture }}" type="list"></profile-picture>
    </a>

    {{-- User information --}}
    <div class="profile-inf">

        {{-- Name --}}
        <a href="{{ url('/') }}/community/{{ $community->id }}">
            <h2 class="verticalList-elementHeading">{{ $community->name }}</h2>
        </a>

        {{-- Location --}}
        <div>{{ displayLocation($community->city, $community->country) }}</div>

        {{-- Subscription --}}
        @if ($community->users->contains(auth()->id()))
            <a href="{{ url('/') }}/community/{{ $community->id }}/unsubscribe">Unsubscribe</a>
        @else
            <a href="{{ url('/') }}/community/{{ $community->id }}/subscribe">Subscribe</a>
        @endif

    </div>

    {{-- Clear --}}
    <div class="clear"></div>

    {{-- Add advert button --}}
    @if (auth()->id())
        <a class="addCommunity addButton" href="{{ '/communities/create' }}"></a>
    @endif

</div>