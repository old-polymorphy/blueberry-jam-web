@extends('layouts.app')

@section('title')
    Reset Password
@endsection

@section('content')

    {{-- Flash --}}
    @if (session('status'))
        <div class="flash">
            {{ session('status') }}
        </div>
    @endif

    {{-- Form --}}
    <form method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        <section>

            {{-- E-mail --}}
            <div class="form-group{{ $errors->has('email') ? ' alert' : '' }}">
                <input placeholder="E-mail Adress" type="email" name="email" value="{{ $email or old('email') }}"
                       required
                       autofocus>
                @if ($errors->has('email'))
                    <ul class="errorList">
                        @foreach ($errors->get('email') as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>

            {{-- Password --}}
            <div class="form-group{{ $errors->has('password') ? ' alert' : '' }}">
                <input placeholder="Password" type="password" name="password" required>
                @if ($errors->has('password'))
                    <ul class="errorList">
                        @foreach ($errors->get('password') as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' alert' : '' }}">
                <input placeholder="Confirm password" type="password" name="password_confirmation" required>
                @if ($errors->has('password_confirmation'))
                    <ul class="errorList">
                        @foreach ($errors->get('password_confirmation') as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>

        </section>

        {{-- Button --}}
        <div class="form-group">
            <button type="submit" class="button">Reset Password</button>
        </div>

    </form>
@endsection
