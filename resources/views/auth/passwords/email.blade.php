@extends('layouts.app')

@section('title')
    Reset Password
@endsection

@section('content')

    {{-- Flash --}}
    @if (session('status'))
        <div class="flash">
            {{ session('status') }}
        </div>
    @endif

    {{-- Form --}}
    <form method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}

        <section>

            {{-- E-mail --}}
            <div class="form-group{{ $errors->has('email') ? ' alert' : '' }}">
                <input type="email" name="email" placeholder="E-mail Address" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <ul class="errorList">
                        @foreach ($errors->get('email') as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>

        </section>

        {{-- Button --}}
        <div class="form-group">
            <button type="submit" class="button">Send Link</button>
        </div>
    </form>
@endsection
