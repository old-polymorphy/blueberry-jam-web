{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', 'Sign Up')

{{-- Content --}}
@section('content')

    {{-- Register form --}}
    <form class="auth form loginForm" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <section>

            {{-- Username --}}
            <div class="form-group{{ $errors->has('name') ? ' alert' : '' }}">
                <input aria-label="username" placeholder="Username" type="text" name="name" value="{{ old('name') }}"
                       required autofocus>
            </div>

            {{-- Error block --}}
            @if ($errors->has('name'))
                <ul class="errorList">
                    @foreach ($errors->get('name') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {{-- E-mail --}}
            <div class="form-group{{ $errors->has('email') ? ' alert' : '' }}">
                <input aria-label="email" placeholder="E-mail" type="email" name="email" value="{{ old('email') }}"
                       required>
            </div>

            {{-- Error block --}}
            @if ($errors->has('email'))
                <ul class="errorList">
                    @foreach ($errors->get('email') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {{-- Password --}}
            <div class="form-group{{ $errors->has('password') ? ' alert' : '' }}">
                <input aria-label="password" placeholder="Password" type="password" name="password" required>
            </div>

            {{-- Error block --}}
            @if ($errors->has('password'))
                <ul class="errorList">
                    @foreach ($errors->get('password') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {{-- Password confirmation --}}
            <div class="form-group">
                <div>
                    <input aria-label="confirmation" placeholder="Password confirmation" type="password"
                           name="password_confirmation" required>
                </div>
            </div>

        </section>

        {{-- Submit --}}
        <div class="form-group">
            <button class="button" type="submit">Register</button>
        </div>

    </form>

@endsection
