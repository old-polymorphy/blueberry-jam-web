{{-- Parent layout --}}
@extends('layouts.app')

{{-- Title --}}
@section('title', 'Sign In')

{{-- Content --}}
@section('content')

    {{-- Auth form --}}
    <form class="auth form registerForm" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <section>

            {{-- E-mail --}}
            <div class="form-group{{ $errors->has('email') ? ' alert' : '' }}">
                <input aria-label="email" placeholder="E-mail" type="email" name="email" value="{{ old('email') }}"
                       required autofocus>
            </div>

            {{-- Error block --}}
            @if ($errors->has('email'))
                <ul class="errorBlock">
                    @foreach ($errors->get('email') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {{-- Password --}}
            <div class="form-group{{ $errors->has('password') ? ' alert' : '' }}">
                <input aria-label="password" placeholder="Password" type="password" name="password" required>
            </div>

            {{-- Rendering errors --}}
            @if ($errors->has('password'))
                <ul class="errorBlock">
                    @foreach ($errors->get('password') as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {{-- Remember --}}
            <div class="form-group customCheckbox">
                <input class="customCheckbox-input" id="loginForm-rememberMe" type="checkbox" name="remember">
                <label class="customCheckbox-label" for="loginForm-rememberMe">Remember me</label>
            </div>

        </section>

        {{-- Submit --}}
        <div class="form-group">
            <button class="button" type="submit">Sign in</button>
            <a href="{{ route('password.request') }}">Forgot your password?</a>
        </div>

    </form>

@endsection
