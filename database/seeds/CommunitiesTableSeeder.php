<?php

use Illuminate\Database\Seeder;

class CommunitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('communities')->insert([
            [
                'name' => 'Polymorphy',
                'city' => 'Moscow',
                'country' => 'Russia',
                'picture' => '',
                'creator' => 1,
            ],
        ]);
    }
}
