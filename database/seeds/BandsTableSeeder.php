<?php

use Illuminate\Database\Seeder;

class BandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bands')->insert([
            [
                'name' => 'Placebo',
                'city' => 'London',
                'country' => 'UK',
                'picture' => ''
            ],
            [
                'name' => 'Dopamine',
                'city' => 'Moscow',
                'country' => 'Russia',
                'picture' => ''
            ]
        ]);
    }
}
