<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
            [
                'name' => 'blues'
            ],
            [
                'name' => 'classical'
            ],
            [
                'name' => 'jazz'
            ],
            [
                'name' => 'pop'
            ],
            [
                'name' => 'rap'
            ],
            [
                'name' => 'rock'
            ]
        ]);
    }
}
