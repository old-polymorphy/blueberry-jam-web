<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'name' => 'ivan.matcuka',
                'email' => 'ivan.mazuka@yandex.ru',
                'password' => bcrypt('HnRtdr3zss'),
            ],
            [
                'name' => 'evan.mathews',
                'email' => 'evanandrewmathews@gmail.com',
                'password' => bcrypt('HnRtdr3zss'),
            ],
            [
                'name' => str_random(10),
                'email' => str_random(10) . '@gmail.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => str_random(10),
                'email' => str_random(10) . '@gmail.com',
                'password' => bcrypt('secret'),
            ],
            [
                'name' => str_random(10),
                'email' => str_random(10) . '@gmail.com',
                'password' => bcrypt('secret'),
            ],
        ]);
    }
}
