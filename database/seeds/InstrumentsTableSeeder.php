<?php

use Illuminate\Database\Seeder;

class InstrumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instruments')->insert([
            [
                'name' => 'bass'
            ],
            [
                'name' => 'drums'
            ],
            [
                'name' => 'guitar'
            ],
            [
                'name' => 'piano'
            ],
            [
                'name' => 'vocal'
            ]
        ]);
    }
}
