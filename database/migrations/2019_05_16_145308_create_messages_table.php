<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('messages', static function (Blueprint $table) {
            $table->increments('id');

            // Addressee
            $table->unsignedInteger('addressee');
            $table
                ->foreign('addressee')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            // Destination
            $table->unsignedInteger('destination');
            $table
                ->foreign('destination')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('messages');
    }
}
