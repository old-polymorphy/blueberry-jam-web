<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instruments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        // Pivot
        Schema::create('instrument_user', function (Blueprint $table) {
            $table->integer('instrument_id')->unsigned()->nullable()->references('id')->on('instruments')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable()->references('id')->on('users')->onDelete('cascade');
            $table->primary(array('instrument_id', 'user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instruments');
        Schema::dropIfExists('instrument_user');
    }
}
