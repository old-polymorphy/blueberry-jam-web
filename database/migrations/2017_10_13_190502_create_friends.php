<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriends extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friends', function (Blueprint $table) {
            $table->integer('addressee')->unsigned();
            $table->integer('destination')->unsigned();
            $table->foreign('addressee')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('destination')->references('id')->on('users')->onDelete('cascade');
            $table->primary(array('addressee', 'destination'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friends');
    }
}
