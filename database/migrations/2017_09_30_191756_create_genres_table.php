<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
        });

        // Pivot
        Schema::create('genre_user', function (Blueprint $table) {
            $table->integer('genre_id')->unsigned()->nullable()->references('id')->on('genres')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable()->references('id')->on('users')->onDelete('cascade');
            $table->primary(array('genre_id', 'user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genres');
        Schema::dropIfExists('genre_user');
    }
}
