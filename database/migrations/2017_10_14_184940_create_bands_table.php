<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->text('bio')->nullable();
            $table->string('picture')->nullable();
            $table->dateTime('ban')->nullable();
            $table->integer('creator')
                  ->unsigned()
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade')
                  ->default(1);
            $table->rememberToken();
            $table->timestamps();
        });

        // Pivot table
        Schema::create('band_user', function (Blueprint $table) {
            $table->integer('band_id');
            $table->integer('user_id');
            $table->tinyInteger('role')->default(0);
            $table->primary(array('band_id', 'user_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bands');
        Schema::dropIfExists('band_user');
    }
}
